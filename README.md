# A survey on tree indexing problems - implementation

This repository contains the implementation of the practical part of the bachelor thesis.

The program implements three different solutions to the subtree rejection problem.
The solutions are subtree pushdown automata, subtree oracle pushdown automata, and subtree finite automata.

The program has to be run with the following arguments:
    `./bap indextype <treefile> <queryfile>`.

Index type has to be one of the three implemented solutions – subtreePDA, subtreeFA, or subtreeOraclePDA.
Instead of one query file, we can insert more query files ` -qN queryfile1 queryfile2 ... queryfileN`.
To specify the log file, we can add the file's name as the last argument.
The log file has four columns. The first column states the build-time of the index in CPU ms, the second column states the number of states, the third number of transitions, and the fourth is the time it took to complete
all the queries.

To run an experiment for an already generated dataset, we can run the experimentsTime.sh or experimentsMemory.sh
script, based on which data we prefer to measure.
The memory consumption is calculated as the peak memory consumption in bytes.

Beware that the program is meant for testing purposes only; that is, the input is, for example, not checked for valid values.

To build the program, we need `make`, `g++`, and optionally `valgrind` packages.