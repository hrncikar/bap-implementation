CXX = g++
LD = g++
CXXFLAGS = -std=c++17 -D=LOG_ENABLE -Wall -pedantic -O2
LDFLAGS = -std=c++17 -D=LOG_ENABLE -Wall -pedantic -O2
DEBUG_FLAGS = -Wextra -D=LOG_ENABLE -pedantic -g
MEMORY_FLAGS = -Wextra -pedantic -Wextra -g -O2
MEM_SANITIZE = -fsanitize=address
SRC = src
OBJECT_DIR = bin
OBJECT_FILES = $(addprefix $(OBJECT_DIR)/, $(notdir $(patsubst %.cpp, %.o, $(wildcard  $(SRC)/*.cpp))))
# .d file for each .o file
DEPS = $(OBJECT_FILES:.o=.d)
TARGET = bap

# add for tree generator implementation

.PHONY: all compile clean deps memsanitize debug

all: compile

compile: $(TARGET)

clean:
	rm -rf $(OBJECT_DIR)

deps:
	$(CXX) -MM $(SRC)/*.cpp > Makefile.d

memsanitize: CXXFLAGS += $(MEM_SANITIZE)
memsanitize: LDFLAGS += $(MEM_SANITIZE)
memsanitize: all

debug: CXXFLAGS += $(DEBUG_FLAGS)
debug: all

memory: CXXFLAGS += $(MEMORY_FLAGS)
memory: all

$(TARGET): $(OBJECT_FILES)
	$(LD) $(LDFLAGS) $^ -o $@

$(OBJECT_DIR)/%.o: $(SRC)/%.cpp | $(OBJECT_DIR)
	@# -MMD creates for each .o file a .d file as a by product of compilation (make deps does not have to be called before)
	$(CXX) $(CXXFLAGS) -MMD -c $< -o $@ 

$(OBJECT_DIR):
	mkdir -p $@

# includes all .d, thefore tracts changes in .hpp too
-include $(DEPS)
