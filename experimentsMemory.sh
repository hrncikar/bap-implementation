#!/bin/sh

# peak memory consumption test, linux only 

make clean
make memory

results=data/experiment/results
counter=1

memoryTest ()
{
    queries=$1
    trees=$2

    for f in "$trees"/* 
    do
        valgrind --tool=massif --massif-out-file="$results"/tmpS.txt ./bap subtreePDA       "$f" "$queries" &> /dev/null
        valgrind --tool=massif --massif-out-file="$results"/tmpO.txt ./bap subtreeOraclePDA "$f" "$queries" &> /dev/null
        valgrind --tool=massif --massif-out-file="$results"/tmpF.txt ./bap subtreeFA        "$f" "$queries" &> /dev/null

        grep -Eo -e 'mem_heap_B=([0-9])*' "$results"/tmpS.txt | cut -d "=" -f 2 | sort -g | tail -n 1 >> "$results"/memorySubtreePDA.txt
        grep -Eo -e 'mem_heap_B=([0-9])*' "$results"/tmpO.txt | cut -d "=" -f 2 | sort -g | tail -n 1 >> "$results"/memoryOraclePDA.txt
        grep -Eo -e 'mem_heap_B=([0-9])*' "$results"/tmpF.txt | cut -d "=" -f 2 | sort -g | tail -n 1 >> "$results"/memorySubtreeFA.txt
        echo "MEMORY: Tree $counter done"
        counter=$(($counter + 1))
    done
}

memoryTest data/experiment/dataset/queries/queriesD01.txt data/experiment/dataset/trees/treesD0001 

memoryTest data/experiment/dataset/queries/queriesD02.txt data/experiment/dataset/trees/treesD0003

memoryTest data/experiment/dataset/queries/queriesD03.txt data/experiment/dataset/trees/treesD0007

memoryTest data/experiment/dataset/queries/queriesD04.txt data/experiment/dataset/trees/treesD0011

memoryTest data/experiment/dataset/queries/queriesD06.txt data/experiment/dataset/trees/treesD0015


make clean

rm "$results"/tmpO.txt
rm "$results"/tmpS.txt
rm "$results"/tmpF.txt
