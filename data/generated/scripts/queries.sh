#!/bin/bash
a=25

#----------------------------------------------------------------------------------------------------------------------
treefile=queriesD01.txt
sizefile=sizequeriesD01.txt
# for tree depth 1
for i in {1..20}
do
  ./aql2 -c "print tree::generate::RandomRankedTreeFactory 0 1 $a 0 $a | string::Compose -" >> $treefile
  echo "0 1" >> $sizefile
  for j in {2..5}
  do
    ./aql2 -c "print tree::generate::RandomRankedTreeFactory 1 $j $a 0 $a | string::Compose -" >> $treefile
    echo "1 $j" >> $sizefile
  done
done

#----------------------------------------------------------------------------------------------------------------------
treefile=queriesD02.txt
sizefile=sizequeriesD02.txt
# for tree depth 2,3
for i in {1..7}
do
  ./aql2 -c "print tree::generate::RandomRankedTreeFactory 0 1 $a 0 $a | string::Compose -" >> $treefile
  echo "0 1" >> $sizefile
  for j in {2..5}
  do
    ./aql2 -c "print tree::generate::RandomRankedTreeFactory 1 $j $a 0 $a | string::Compose -" >> $treefile
    echo "1 $j" >> $sizefile
  done

  for j in {3..10}
  do
    ./aql2 -c "print tree::generate::RandomRankedTreeFactory 2 $j $a 0 $a | string::Compose -" >> $treefile
    echo "2 $j" >> $sizefile
  done
done

for j in {3..11}
do
    ./aql2 -c "print tree::generate::RandomRankedTreeFactory 2 $j $a 0 $a | string::Compose -" >> $treefile
    echo "2 $j" >> $sizefile
done

#----------------------------------------------------------------------------------------------------------------------
treefile=queriesD03.txt
sizefile=sizequeriesD03.txt
# for tree depth 4..7
for i in {1..4}
do
  ./aql2 -c "print tree::generate::RandomRankedTreeFactory 0 1 $a 0 $a | string::Compose -" >> $treefile
  echo "0 1" >> $sizefile
  for j in {2..3}
  do
    ./aql2 -c "print tree::generate::RandomRankedTreeFactory 1 $j $a 0 $a | string::Compose -" >> $treefile
    echo "1 $j" >> $sizefile
  done

  for j in {3..10}
  do
    ./aql2 -c "print tree::generate::RandomRankedTreeFactory 2 $j $a 0 $a | string::Compose -" >> $treefile
    echo "2 $j" >> $sizefile
  done

  for j in {4..17}
  do
    ./aql2 -c "print tree::generate::RandomRankedTreeFactory 3 $j $a 0 $a | string::Compose -" >> $treefile
    echo "3 $j" >> $sizefile
  done
done
#----------------------------------------------------------------------------------------------------------------------
treefile=queriesD04.txt
sizefile=sizequeriesD04.txt
# for tree depth 8..11
for i in {1..3}
do
  ./aql2 -c "print tree::generate::RandomRankedTreeFactory 0 1 $a 0 $a | string::Compose -" >> $treefile
  echo "0 1" >> $sizefile
  for j in {2..4}
  do
    ./aql2 -c "print tree::generate::RandomRankedTreeFactory 1 $j $a 0 $a | string::Compose -" >> $treefile
    echo "1 $j" >> $sizefile
  done

  for j in {3..10}
  do
    ./aql2 -c "print tree::generate::RandomRankedTreeFactory 2 $j $a 0 $a | string::Compose -" >> $treefile
    echo "2 $j" >> $sizefile
  done

  for j in {6..15}
  do
    ./aql2 -c "print tree::generate::RandomRankedTreeFactory 3 $j $a 0 $a | string::Compose -" >> $treefile
    echo "3 $j" >> $sizefile
  done  
done

for j in {5..38}
do
  ./aql2 -c "print tree::generate::RandomRankedTreeFactory 4 $j $a 0 $a | string::Compose -" >> $treefile
  echo "4 $j" >> $sizefile
done
#----------------------------------------------------------------------------------------------------------------------
treefile=queriesD06.txt
sizefile=sizequeriesD06.txt
# for tree depth 12..15

./aql2 -c "print tree::generate::RandomRankedTreeFactory 0 1 $a 0 $a | string::Compose -" >> $treefile
echo "0 1" >> $sizefile
for j in {2..4}
do
  ./aql2 -c "print tree::generate::RandomRankedTreeFactory 1 $j $a 0 $a | string::Compose -" >> $treefile
  echo "1 $j" >> $sizefile
done

for j in {3..10}
do
  ./aql2 -c "print tree::generate::RandomRankedTreeFactory 2 $j $a 0 $a | string::Compose -" >> $treefile
  echo "2 $j" >> $sizefile
done

for j in {8..15}
do
  ./aql2 -c "print tree::generate::RandomRankedTreeFactory 3 $j $a 0 $a | string::Compose -" >> $treefile
  echo "3 $j" >> $sizefile
done  

for j in {12..33}
do
  ./aql2 -c "print tree::generate::RandomRankedTreeFactory 4 $j $a 0 $a | string::Compose -" >> $treefile
  echo "4 $j" >> $sizefile
done

for j in {37..66}
do
  ./aql2 -c "print tree::generate::RandomRankedTreeFactory 5 $j $a 0 $a | string::Compose -" >> $treefile
  echo "5 $j" >> $sizefile
done

for j in {73..128..2}
do
  ./aql2 -c "print tree::generate::RandomRankedTreeFactory 6 $j $a 0 $a | string::Compose -" >> $treefile
  echo "6 $j" >> $sizefile
done

#----------------------------------------------------------------------------------------------------------------------
treefile=queriesD10.txt
sizefile=sizequeriesD10.txt
# for tree depth 20..39

./aql2 -c "print tree::generate::RandomRankedTreeFactory 0 1 $a 0 $a | string::Compose -" >> $treefile
echo "0 1" >> $sizefile
for j in {2..6}
do
  ./aql2 -c "print tree::generate::RandomRankedTreeFactory 1 $j $a 0 $a | string::Compose -" >> $treefile
  echo "1 $j" >> $sizefile
done

for j in {3..13}
do
  ./aql2 -c "print tree::generate::RandomRankedTreeFactory 2 $j $a 0 $a | string::Compose -" >> $treefile
  echo "2 $j" >> $sizefile
done

for d in {3..5}
do
  p=$((2 ** ($d + 1) - 10))
  for i in {1..11}
  do
    ./aql2 -c "print tree::generate::RandomRankedTreeFactory $d $p $a 0 $a | string::Compose -" >> $treefile
    echo "$d $p" >> $sizefile
    p=$(($p+1))
  done
done

for d in {6..10}
do
  p=$((2 ** ($d + 1) - 50))
  for i in {1..10}
  do
    ./aql2 -c "print tree::generate::RandomRankedTreeFactory $d $p $a 0 $a | string::Compose -" >> $treefile
    echo "$d $p" >> $sizefile
    p=$(($p+10))
  done
done

#----------------------------------------------------------------------------------------------------------------------
treefile=queriesD20.txt
sizefile=sizequeriesD20.txt
# for tree depth 40..100
./aql2 -c "print tree::generate::RandomRankedTreeFactory 0 1 $a 0 $a | string::Compose -" >> $treefile
echo "0 1" >> $sizefile
for j in {2..6}
do
  ./aql2 -c "print tree::generate::RandomRankedTreeFactory 1 $j $a 0 $a | string::Compose -" >> $treefile
  echo "1 $j" >> $sizefile
done

for j in {3..11}
do
  ./aql2 -c "print tree::generate::RandomRankedTreeFactory 2 $j $a 0 $a | string::Compose -" >> $treefile
  echo "2 $j" >> $sizefile
done

for d in {3..5}
do
  p=$((2 ** ($d + 1) - 10))
  for i in {1..10}
  do
    ./aql2 -c "print tree::generate::RandomRankedTreeFactory $d $p $a 0 $a | string::Compose -" >> $treefile
    echo "$d $p" >> $sizefile
    p=$(($p+1))
  done
done

for d in {6..10}
do
  p=$((2 ** ($d + 1) - 50))
  for i in {1..9}
  do
    ./aql2 -c "print tree::generate::RandomRankedTreeFactory $d $p $a 0 $a | string::Compose -" >> $treefile
    echo "$d $p" >> $sizefile
    p=$(($p+10))
  done
done

for d in {11..20}
do
    ./aql2 -c "print tree::generate::RandomRankedTreeFactory $d $p $a 0 $a | string::Compose -" >> $treefile
    echo "$d $p" >> $sizefile
    p=$(($p+31))
done
