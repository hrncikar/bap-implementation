#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <cctype>
#include <string>

using namespace std;

int main ( int argc, char * argv[] ) 
{
  if ( argc != 3 )
  {
    cout << argv[0] <<"<fileToParse> <directory>"<< argv[1] << endl;
    return 1;
  }

  ifstream input ( argv[1] );
  if ( ! ( input . is_open () ) )
  {
    cout << "couldn't open the input file "<< argv[1] << endl;
    return 1;
  }

  string prefix = argv[2] + "/tree"s;
  string filetype = ".txt";
  int counter = 1;

  for ( string tree; getline ( input , tree ); ++counter )
  {
    ostringstream oss;
    oss << prefix << setw ( 5 ) << setfill ( '0' ) << counter << filetype; 
    ofstream output ( oss . str () );
    if ( ! ( output . is_open () ) )
    {
      cout << "couldn't open the output file" << endl;
      return 1;
    }
    output << tree << endl;
    output . close ();
  }
  return 0;
}