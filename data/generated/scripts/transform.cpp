#include <iostream>
#include <fstream>
#include <cctype>
using namespace std;

int main ( int argc, char * argv[] ) 
{
  if ( argc != 3 )
  {
    cout << argv[0] <<"<inputfile> <outputfile>"<< argv[1] << endl;
    return 1;
  }
  ifstream input ( argv[1] );
  if ( ! ( input . is_open () ) )
  {
    cout << "couldn't open the input file "<< argv[1] << endl;
    return 1;
  }
  ofstream output ( argv[2] );
  if ( ! ( output . is_open () ) )
  {
    cout << "couldn't open the output file" << endl;
    return 1;
  }
  
  char c;
  int number = 0 ;

  while ( input >> noskipws >> c ) //relies on newline at the endf
  {
    if ( isspace(c) )
    {
      if ( number < 'a' )
        output << number;
      output << c;
      output . flush ();
      number = 0;
    }
    else
    {
      number *= 10;
      number += c - '0' ;
    }
    
  }
  output . close ();
  return 0;
}