#!/bin/bash
a=25
min=2


sizefile=sizetrees1_15.txt
treefile=trees1_15.txt

# Depth 1 - 3 Trees
./aql2 -c "print tree::generate::RandomRankedTreeFactory 1 2 $a 0 $a | string::Compose -" >> $treefile
echo "1 2" >> $sizefile
./aql2 -c "print tree::generate::RandomRankedTreeFactory 1 3 $a 0 $a | string::Compose -" >> $treefile
echo "1 3" >> $sizefile
./aql2 -c "print tree::generate::RandomRankedTreeFactory 1 4 $a 0 $a | string::Compose -" >> $treefile
echo "1 4" >> $sizefile
./aql2 -c "print tree::generate::RandomRankedTreeFactory 1 5 $a 0 $a | string::Compose -" >> $treefile
echo "1 5" >> $sizefile


# Depth 2 - 10 Trees
i=2
p=3
for j in {1..5}
do
  p=$(( $p + 1 ))
  ./aql2 -c "print tree::generate::RandomRankedTreeFactory $i $p $a 0 $p | string::Compose -" >> $treefile
  echo "$i $p" >> $sizefile
  ./aql2 -c "print tree::generate::RandomRankedTreeFactory $i $p $a 0 $p | string::Compose -" >> $treefile
  echo "$i $p" >> $sizefile
done

# Depth 3, 4 - 12 Trees
for i in {3..4}
do
  p=$((2 ** ($i + 1) - (2 ** $i - 1) ))
  for j in {1..6}
  do
    p=$(( $p + 1 ))
    ./aql2 -c "print tree::generate::RandomRankedTreeFactory $i $p $a 0 $p | string::Compose -" >> $treefile
    echo "$i $p" >> $sizefile
  done

  p=$((2 ** ($i + 1) - 1 ))
  for j in {1..6}
  do
    p=$(( $p + 1 ))
    ./aql2 -c "print tree::generate::RandomRankedTreeFactory $i $p $a 0 $p | string::Compose -" >> $treefile
    echo "$i $p" >> $sizefile
  done
done

# Depth 5, 6 - 16 Trees
for i in {5..6}
do
  p=$((2 ** ($i + 1) - (2 ** $i - 1) ))
  for j in {1..8}
  do
    p=$(( $p + 3 ))
    ./aql2 -c "print tree::generate::RandomRankedTreeFactory $i $p $a 0 $p | string::Compose -" >> $treefile
    echo "$i $p" >> $sizefile
  done

  p=$((2 ** ($i + 1) -  1 ))
  for j in {1..8}
  do
    p=$(( $p + 3 ))
    ./aql2 -c "print tree::generate::RandomRankedTreeFactory $i $p $a 0 $p | string::Compose -" >> $treefile
    echo "$i $p" >> $sizefile
  done
done


# Depth 7..15 - 50 Trees
for i in {7..15}
do
  p=$((2 ** ($i + 1) - (2 ** $i - 1) ))
  for j in {1..25}
  do
    p=$(( $p + 5 ))
    ./aql2 -c "print tree::generate::RandomRankedTreeFactory $i $p $a 0 $p | string::Compose -" >> $treefile
    echo "$i $p" >> $sizefile
  done

  p=$((2 ** ($i + 1) -  1 ))
  for j in {1..25}
  do
    p=$(( $p + 5 ))
    ./aql2 -c "print tree::generate::RandomRankedTreeFactory $i $p $a 0 $p | string::Compose -" >> $treefile
    echo "$i $p" >> $sizefile
  done
done


# Depth 16..100 - 10 Trees
#for i in {16..100}
#do
#  for j in {1..5}
#  do
#    p=$(( $p + 63 ))
#    ./aql2 -c "print tree::generate::RandomRankedTreeFactory $i $p $a 0 $p | string::Compose -" >> $treefile
#    echo "$i $p" >> $sizefile
#    ./aql2 -c "print tree::generate::RandomRankedTreeFactory $i $p $a 0 $min | string::Compose -" >> $treefile
#    echo "$i $p" >> $sizefile
#  done
#done
