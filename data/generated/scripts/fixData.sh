g++ -Wall -pedantic -std=c++17 transform.cpp

cut -d " " -f 2- queriesD01.txt > tmp.txt
./a.out tmp.txt queriesD01.txt

cut -d " " -f 2- queriesD02.txt > tmp.txt
./a.out tmp.txt queriesD02.txt

cut -d " " -f 2- queriesD03.txt > tmp.txt
./a.out tmp.txt queriesD03.txt

cut -d " " -f 2- queriesD04.txt > tmp.txt
./a.out tmp.txt queriesD04.txt

cut -d " " -f 2- queriesD06.txt > tmp.txt
./a.out tmp.txt queriesD06.txt

cut -d " " -f 2- queriesD10.txt > tmp.txt
./a.out tmp.txt queriesD10.txt

cut -d " " -f 2- queriesD20.txt > tmp.txt
./a.out tmp.txt queriesD20.txt

cut -d " " -f 2- trees1_100.txt > tmp.txt
./a.out tmp.txt trees.txt

cut -d " " -f 2- trees1_15.txt > tmp.txt
./a.out tmp.txt trees.txt

g++ -Wall -pedantic -std=c++17 separate.cpp
./a.out trees.txt trees

rm a.out
rm tmp.txt
