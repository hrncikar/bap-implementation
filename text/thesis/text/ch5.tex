\chapter{Implementation}
\label{chapter:5}

In this chapter, we describe our C++ implementation of three solutions to the subtree rejection problem presented in Chapter \ref{chapter:4}; that is, subtree pushdown automata, subtree oracle pushdown automata, and subtree finite automata.
We then show how we collected data for our experiments to assess and compare the efficiency of the three algorithms.

The rest of the chapter is organized as follows.
First, we overview our implementation of the indexes.
We describe how we implemented pushdown automata and the two solutions that use them: subtree pushdown automata and subtree oracle pushdown automata.
Then we show our implementation of finite automata and the indexing algorithm subtree finite automata.
Finally, we state how we collected data during the experiments.

%--------------------------------------------------------------------------------------------------
\section{Indexes}

The central object of the whole program is the subtree rejection index.
The object is represented by the \verb|CSubtreeRejectionIndex| abstract class.
Each of the implemented solutions to the subtree rejection index (indirectly) inherits from the class.
There are two methods that all subtree rejection indexes have to implement.
These methods are:
\begin{lstlisting}
virtual void BuildIndex ( const std::string & tree ) = 0;
virtual bool Query      ( const std::string & queryPattern ) = 0;
\end{lstlisting}

The pure virtual  \verb|BuildIndex| method represents the preprocessing of a given tree to create its subtree rejection index.
The aim of the  \verb|Query| method is to answer with a boolean value whether a concrete implementation of a subtree rejection index has found a match to a query pattern within an input tree.

For simplicity, the program accepts only trees labeled with a uniquely ranked alphabet.
Both methods expect trees in their prefix linear notation (see Definition \ref{def:prefix-notation}).
The linear notation is represented by a string of whitespaces and integers.
Each integer represents a node.
The value of an integer is the arity of the corresponding node.
For example, the methods accept trees such as the one illustrated in Figure \ref{img:urot}, whose prefix linear notation is \verb|2 0 1 0|.

\begin{figure}
\centering
\input{diagrams/51}
\caption[Ranked ordered tree with integers for labels]{
A ranked ordered tree with integers for labels.
}
\label{img:urot}
\end{figure}

All three solutions expect a valid input.
We can afford this since the implementation of the algorithms is intended only for experiments, which saves some time not only in the preprocessing phase but also in each query call.

All the implemented solutions are automata, and the interpretation of a state of an automaton is the same across all the indexes; therefore, we created one class to represent a state of both finite and pushdown automata, called \verb|CState|.
The class represents the name of a state by a set of integers \verb|std::set<int>|.
Furthermore, the class contains a method called \verb|AreCorresponding| that implements the logic behind determining whether two states are corresponding according to Definition \ref{def:cs}.

Now we have overviewed the common parts of all the indexes.
Next, we focus on our implementation of pushdown automata, which is the basis for subtree pushdown automata and subtree oracle pushdown automata indexes.


%--------------------------------------------------------------------------------------------------

%--------------------------------------------------------------------------------------------------
\subsection{Pushdown automata}

Both subtree pushdown automata and subtree oracle pushdown automata use only one pushdown store symbol. When that is the case, we can represent the pushdown store of a pushdown automaton by a simple integer counter instead of some container. In the implementation, we call these automata \emph{counter automata} and represent them by the abstract class \verb|CCounterAutomata|.

As for the structure of \verb|CCounterAutomata|, it consists of only the data necessary to represent a PDA.
In order to do so, \verb|CCounterAutomata| defines two supporting structures \verb|TFrom| and \verb|TTo|, which are used to represent the transitions of an automaton.
\verb|TFrom| contains an instance of the \verb|CState| class, an integer representing an input symbol, and an integer representing what ought to be extracted from the counter representing the pushdown store (the value is always set to 1 in the implemented solutions).
\verb|TTo| also contains an instance of \verb|CState| and an integer representing what is added to the counter.
We can think of the two structures as if \verb|TFrom| instances are arguments of $\delta$ and sets of \verb|TTo| instances are its results: $\operatorname{TTo}\in \delta(\operatorname{TFrom})$.

The 7-tuple that defines a PDA is represented in the following way.
The states of an automaton are implemented as a set of states \verb|std::set<CState>|.
The alphabet is represented by a set of integers \verb|std::set<int>|.
There is no attribute to represent the pushdown store alphabet because there is only one symbol in it, and we do not need to know the exact symbol.
Transitions are represented by a map from \verb|TFrom| to a set of \verb|TTo|, such that \verb|std::map<TFrom, std::set<TTo>>|.
The initial state is represented by an instance of the \verb|CState| class.
The initial pushdown store symbol is not represented because the specific pushdown store symbol is unnecessary since we know that there is only one symbol in the pushdown store alphabet; the important part is that there is always only one symbol initially in the pushdown store.
Furthermore, we do not represent the set of final states because both of the implemented pushdown automata accept input strings by an empty pushdown store (in our case, the counter set to 0).
To sum it up, from the 7-tuple that defines a PDA, we need to represent only four of the items because we implement counter automata accepting by an empty pushdown store.

Both \verb|std::map| and \verb|std::set| that we use in our implementation are usually implemented by red-black trees, which means that operations insert, find, and erase have $O(\log(n))$ time complexity, where $n$ is the size of one of those two containers \cite{noauthor_undated-xp, noauthor_undated-kg}.

Most importantly, the \verb|CCounterAutomata| class provides two functionalities: to run a query and to make a PDA deterministic.
Even though the \verb|CCounterAutomata| class is not an index on its own, it inherits from \verb|CSubtreeRejectionIndex|. Since
running a query is the same across all counter automata, \verb|CCounterAutomata| implements the \verb|CSubtreeRejectionIndex::Query| method.
The method reads one integer from the argument query pattern at a time.
If the number of viable transitions, given the input symbol, is not equal to one, the method returns \verb|false|.
If a move to another state cannot happen due to the lack of symbols in the pushdown store (counter), the method returns \verb|false|.
Otherwise, when there are no other input symbols, the method checks whether the pushdown store is empty (the counter is set to zero) and returns the result.
In conclusion, the \verb|Query| method is expected to run on a deterministic pushdown automaton, and running it on a nondeterministic automaton can wrongly output a negative answer.

As for the time complexity of the \verb|Query| method, we did not obtain the expected $O(m)$, where $m$ is the size of a query pattern \cite{Plicka2011-te, Janousek2009-hd}.
In our implementation, we use the \verb|find| method on the \verb|std::map| to search for viable transitions.
The \verb|find| operation is logarithmic in the size of the container; therefore, in our case, it is logarithmic to the number of transitions of an automaton, resulting in overall time complexity $O(m \cdot \log (t))$, where $m$ is the size of a query pattern and $t$ is the number of transitions.

Furthermore, similar to querying, making input-driven acyclic pushdown automata deterministic is the same across all counter automata.
Therefore, the \verb|CCounterAutomata| class has a~\verb|Determinize| method that implements Algorithm \ref{algo:deterministic-pda}.
The method also deletes the automaton's inaccessible states and transitions, apart from making it deterministic.

Subtree pushdown automata indexes are represented by the \verb|CSubtreePDA| class.
The class inherits from \verb|CCounterAutomata| and implements the  \verb|CSubtreeRejectionIndex::BuildIndex| method.
The method implements Algorithm \ref{algo:n-subtree-pda}, which constructs a subtree PDA for a given tree and calls the \verb|Determinize| method.
For each tree, the method needs to be called once.

Subtree pushdown automata not only provide a binary answer on whether a subtree query is present in an input tree, but they can also tell where to find the occurrences.
We can acquire the locations because of the following properties.
To get into any one state of a subtree PDA, we always move through transitions that contain the same symbol on the input.
The same holds for our implementation of a subtree PDA.
All states of \verb|CSubtreePDA| have a name in the form of a set of integers.
These integers correspond to the locations in an input tree where we can find nodes with a label equal to the input symbol with which we got into a state.
For example, let us have a state of an automaton labeled $\{4,6,12\}$ into which we can only get through transitions that have as an input symbol $a$.
Then the corresponding nodes in the input tree that the state represents are at positions 4, 6, and 12, and their label is $a$.
However, in the implementation, we do not utilize this property of the index since we build subtree rejection indexes that need only binary answers.

Subtree oracle pushdown automata are implemented as a \verb|COraclePDA| class.
The \verb|COraclePDA| class inherits from \verb|CSubtreePDA| and implements \verb|CSubtreeRejectionIndex::BuildIndex| where it creates a subtree PDA and then merges its corresponding states; lastly, it calls the \verb|Determinize| method.
Within the \verb|BuildIndex| method, we rename the states of the subtree PDA; therefore, we cannot obtain the location of a subtree in the input data tree.



%--------------------------------------------------------------------------------------------------

%--------------------------------------------------------------------------------------------------
\subsection{Deterministic finite automata}

Finite automata pose a similar role as counter automata in the implementation in that they are not an index themselves, but instead, they build the structure of an automaton.
Deterministic finite automata are represented by a \verb|CDeterministicFA| abstract class that inherits from \verb|CSubtreeRejectionIndex|.

Similarly to \verb|CCounterAutomata|, it defines two supporting classes, \verb|TFrom| and \verb|TTo|, to help represent transitions of an automaton $\delta(\operatorname{TFrom}) = \operatorname{TTo}$.
Both \verb|TFrom| and \verb|TTo| contain an instance of \verb|CState|, and \verb|TFrom| additionally contains an integer representing a symbol on the input.

In order to represent a finite automaton, \verb|CDeterministicFA| contains four member variables.
Two variables that represent all the states of an automaton and all the final states, both are of type \verb|std::set<CState>|.
To represent the transitions of an automaton, it contains a member variable of type \verb|std::map<TFrom,TTo>|.
To represent the initial state, it contains an instance of \verb|CState|.
We do not need to represent the alphabet of a finite automaton since subtree finite automaton does not need it during the building phase (unlike pushdown automata indexes that need it for determinization).

\verb|CDeterministicFA| provides the implementation of the \verb|CSubtreeRejectionIndex:Query| method.
The method returns \verb|false| when there is no viable transition on the current state and input symbol or when the automaton ends in a non-final state.
Regarding time complexity, the implementation of the \verb|Query| method faces the same drawback as in \verb|CCounterAutomata|.
Because we use the \verb|find| method on a \verb|std::map| the time complexity is $O(m \cdot \log (t))$ instead of $O(m)$, where $m$ is the size of a query pattern and $t$ is the number of transitions. 

Analogously to subtree pushdown automata, subtree finite automata indexes are represented by the \verb|CSubtreeFA| class that inherits from \verb|CDeterministicFA| and implements the \verb|CSubtreeRejectionIndex::BuildIndex| method.
In the \verb|BuildIndex| method, we implement Algorithm \ref{algo:dfa}.
To acquire the $\operatorname{subtreeSet}(\operatorname{pref}(T))$ of a tree $T$, we implemented a \verb|subtreeSet| method that for each node of $T$ finds a subtree that treats the node as its root.
In order to find the prefix notation of each subtree of $T$, we calculate the arity checksum beginning in the temporary root node, continuing until the arity checksum is equal to zero, meaning that the found substring represents a subtree in its prefix notation.


%--------------------------------------------------------------------------------------------------

%--------------------------------------------------------------------------------------------------
\section{Experiments}
The aim of the implementation is to compare and contrast the performance of the implemented solutions to the subtree rejection problem.
In order to assess the efficiency of the algorithms, we decided to measure the following data:
\begin{itemize}
    \item the build time, 
    \item the query time, 
    \item the memory consumption, 
    \item the number of an automaton's states, and
    \item the number of an automaton's transitions.
\end{itemize}

The build time, the average query time, and the memory consumption of an indexing algorithm are relevant to all tree indexing solutions.
The rest of the data are specific to automata solutions.
The latter two are simple integers that represent the number of instances of the two factors.
In the rest of this section, we state how precisely we measure the time and memory consumption of the program.

The time data are measured in a separate run from the run analyzing memory because computing the memory consumption of a C++ program negatively affects its time duration.
Both data are collected on a program compiled with the \verb|O2| flag.
\verb|O2| flag means that the code is optimized as much as possible without a space-speed trade-off \cite{noauthor_undated-yc}.

The metric we use to measure the build and query time is the processor time used by these operations, calculated using \verb|std::clock| \cite{noauthor_undated-ho}.
We measure the build time by timing how long it takes to complete a call to the \verb|BuildIndex| method for each tree separately.
Unlike the build time, we measure the query time by calculating the time it takes to complete all queries on an index made for one tree.
Measurement includes the \verb|for| loop to iterate over all queries and to output the results.
Each query is run ten times.
Since all three indexes are tested under the same conditions, we can use these data to compare their performance.

We measure the memory consumption of the different indexes by the maximum allocated bytes on the heap at one time.
To measure the peak memory consumption, we use Valgrind's heap profiler \emph{Massif} \cite{The_Valgrind_Developers_undated-cm}.
Massif calculates the memory consumption of the whole program; therefore, the peak memory consumption does not reflect only the size of a subtree rejection index but also the memory used to store other data.
However, since the memory is determined for each automaton under the same circumstances, we can compare the obtained values.

%--------------------------------------------------------------------------------------------------
