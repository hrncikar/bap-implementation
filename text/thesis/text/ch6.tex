\chapter{Experiment results}
In this chapter, we present experiments that compare our implementations of three solutions to the subtree rejection problem.
The solutions are: subtree pushdown automata, subtree oracle pushdown automata, and subtree finite automata.
To compare and assess the performance of the three algorithms, we measure the following data:
\begin{itemize}
    \item the CPU build time in milliseconds,
    \item the CPU query time in milliseconds,
    \item the peak memory consumption of the program in bytes,
    \item the number of an automaton's states, and
    \item the number of an automaton's transitions.
\end{itemize}

The implementation of the algorithms and the collection of the experiment results are described in Chapter \ref{chapter:5}.
In the rest of this chapter, we present the data on which the three indexes were tested and outline the results of the experiments.

To test the indexing algorithms on a wide range of different sizes and heights of trees and queries, we generated a dataset using the \emph{Algorithms Library Toolkit} \cite{Travnicek_undated-ur}.
The generated dataset was rewritten into a format where each node is represented by an integer representing the node's rank.
Therefore, all trees and queries are over a unique rank alphabet.
The highest rank in the dataset is 21; however, most nodes have a much lower rank.

Altogether, there are 520 different trees.
The lowest height of a tree is 1, and the highest is 15.
For each height, the dataset has sparse as well as dense trees; therefore, 15 was chosen as the maximum height because making dense trees with larger heights would require exponentially more nodes.
The lowest number of nodes in a tree is 2, and the highest is 65 660.
The number of nodes in a tree varies from around $2^{h+1} - 2^h$ to around $2^{h+1}$, where $h$ is the height of a tree.

As for the queries, there are five distinct datasets, each with 100 trees, so that higher trees can have larger query trees.
The datasets differ in the maximum height of a tree, and the heights are 1, 2, 3, 4, and 6.
The minimum size of a query tree is 1, and the maximum size is 127.
The query trees are represented in the same way as the input trees; therefore, they are all over a~unique rank alphabet.
The maximum rank of a node is 10. The pairings of trees and query sets are as follows:
\begin{itemize}
    \item trees of height equal to 1 are queried with trees of height up to 1,
    \item trees of height equal to 2 and 3 are queried with trees of height up to 2,
    \item trees of height equal to 4 to 7 are queried with trees of height up to 3,
    \item trees of height equal to 8 to 11 are queried with trees of height up to 4, and
    \item trees of height equal to 12 to 15 are queried with trees of height up to 6.
\end{itemize}
In Figure \ref{img:info-data}, we can see a graph showing the distribution of the number of nodes in a tree and the height of a tree for the input trees dataset.
The graph shows that as the trees increase in height, they also have more nodes.
Furthermore, we can see that for most of the heights, the trees are divided into two groups, one with a lower number of nodes and one with more nodes.


\begin{figure}
\centering
\includegraphics[width=\textwidth * 3/4]{graphs/info.pdf}
\caption[Distribution of heights and sizes of input trees]{
A graph showing the distribution of heights and sizes of trees in the input trees dataset.
}
\label{img:info-data}
\end{figure}

Across all the implemented automata, subtree finite automata consistently had by far the most states and transitions.
The difference in the number of states and transitions of subtree pushdown automata and subtree oracle pushdown automata was not as significant.
Subtree oracle pushdown automata have $n + 1$ states, where $n$ is the size of a tree \cite{Plicka2011-te}.
However, that is the number of states of a nondeterministic automaton; therefore, the difference between deterministic subtree pushdown automata and deterministic subtree oracle pushdown automata was not as large as we first anticipated.
Nevertheless, subtree oracle pushdown automata consistently had the least states and transitions.
Furthermore, as the input trees got larger, so did the difference in the number of states and transitions between subtree finite automata and the other two automata.
To demonstrate, the subtree FA index for the last input tree had 658 557 states, while the subtree PDA had only 72 212 and the subtree oracle PDA 68 220 states. 

Figures \ref{img:s-s} and \ref{img:s-h} show the relationship between the number of states of an automaton and the height and size of a tree.
Both figures indicate that the number of states is primarily dependent on the size of the input tree, not on its height.
As we can see in Figure \ref{img:s-s}, the number of states corresponds directly to the number of nodes in a tree.
In Figure \ref{img:s-h}, we can see the general trend that the number of states grows, as does the height of a tree. However, from a certain point onward, there is a gap for each height, which suggests that the number of states corresponds more to the size of an input tree than its height. 
The graphs showing the relationship between the number of transitions and the height and size of a tree are nearly identical to the graphs in Figures \ref{img:s-s} and \ref{img:s-h}.
Thus, we placed them in the Appendix.

\begin{figure}
\centering
\includegraphics[width=\textwidth * 3/4]{graphs/s-s.pdf}
\caption[Number of states of automata given the size of an input tree]{
The number of states of the three implemented automata given the size of an input tree.
}
\label{img:s-s}
\end{figure}


\begin{figure}
\centering
\includegraphics[width=\textwidth * 3/4]{graphs/s-h.pdf}
\caption[Number of states of automata given the height of an input tree]{
The number of states of the three implemented automata given the height of an input tree.
}
\label{img:s-h}
\end{figure}

\begin{figure}
\includegraphics[width=\textwidth]{graphs/qt-s.pdf}
\caption[Query time given the size of an input tree]{
The time it took to complete all the queries given the number of nodes in an input tree.
We can see that apart from a few outliers, the query time is proportional to the size of an input tree.
}
\label{img:qt-s}
\end{figure}

\begin{figure}
\includegraphics[width=\textwidth]{graphs/qt-h.pdf}
\caption[Query time given the height of an input tree]{
The time it took to complete all the queries given the height of an input tree.
We believe that for subtree pushdown automata and subtree oracle pushdown automata, the height of a tree does not contribute to the query time as much as the size of a tree.
We draw this conclusion from the fact that the query time distribution for the two automata approximately follows the distribution of the number of nodes given a specific height of an input tree, as is seen in Figure \ref{img:s-h}).
}
\label{img:qt-h}
\end{figure}

It appears that the time it took to complete the queries depended primarily on the size of an input tree for all three solutions.
All implemented indexing solutions have their query time equal to $O(m \cdot \log(t))$, where $m$ is the size of a query and $t$ is the number of transitions in a specific automaton.
Therefore, since the query time was also affected by the number of transitions in an automaton, it is unsurprising that the query time rose even when the query sets were the same.
Thus, we anticipated that subtree finite automata would have the longest query time, as they consistently had by far the most transitions.
Nevertheless, they performed by far the best in this regard.
The second least time took subtree oracle pushdown automata, and the most time took subtree pushdown automata.

We believe that one reason why subtree pushdown automata performed the worst in the query time is that there is inefficiency in naming the states.
Although all the implemented automata use the same class to represent an automaton's state, subtree oracle pushdown automata and subtree finite automata always have only one integer in the set that describes the label of a state.
In contrast, subtree pushdown automata can have multiple integers in the label set because each of the integers represents a position within the input tree.
This might negatively affect the query time because the comparison of any two states takes longer.

However, both subtree oracle pushdown automata and subtree finite automata have label sets of size one, and yet subtree finite automata took less time.
We believe that this is because subtree oracle pushdown automata had to compute and work with pushdown stores, while subtree finite automata did not.

In terms of build time, the subtree oracle pushdown automata took the most time, and subtree finite automata the least, as is seen in Figure \ref{img:bt-h}.
Subtree oracle pushdown automata inherently take longer to build than subtree pushdown automata.
The reason is that the process of building a subtree oracle PDA first creates a subtree PDA and then modifies it.
Subtree finite automata performed the best in the build time, presumably because there were no further necessary steps once an automaton was created.
The algorithm we used created a deterministic finite automaton right away; therefore, no determinization or removal of inaccessible states was needed, in contrast to pushdown automata solutions.

\begin{figure}
\includegraphics[width=\textwidth]{graphs/bt-h.pdf}
\caption[Build time given the height of an input tree]{
The time it took to build an index given the height of an input tree.
}
\label{img:bt-h}
\end{figure}

Unfortunately, the information about the peak memory consumption of the programs, which we can see in Figure \ref{img:mc-h}, is not as relevant as we had hoped.
The primary reason why we question the relevancy of the data is that subtree oracle pushdown automata performed the worst of the three solutions despite the fact that subtree pushdown automata had overall more states, transitions, and the labels of its states can have multiple integers, which we expected to be reflected in the peak memory consumption information.
Moreover, subtree finite automata performed the best even though they have many more states than the other two pushdown automata indexes.
Both of those examples show that the peak memory consumption does not reflect only the size of the resulting index but also the memory consumption of creating the index.
Therefore, unless we have constrained memory space when building the index, the data indicating the peak memory consumption have to be taken with some reservations. 

\begin{figure}
\includegraphics[width=\textwidth]{graphs/mc-h.pdf}
\caption[Memory consumption given the height of a tree]{
The peak memory consumption of a program running an automaton index for a tree with a given height.
}
\label{img:mc-h}
\end{figure}


These experiments provided us with some basic insight into how well the indexes perform in practice.
Of the three indexes that we tested, subtree finite automata performed best with regard to query time, build time, and even peak memory consumption; however, peak memory consumption does reflect only the size of the indexing structure; therefore, it has to be taken with some reservations.
For future work, it would be useful to investigate further how the three indexes perform under these scenarios:
\begin{itemize}
    \item on a wider range of sizes of trees of the same height,
    \item on ranked trees over a non-unique rank alphabet,
    \item on trees in which we could affect the frequency of certain subtrees,
    \item on tests specifically designed to investigate how common are false positives in subtree oracle pushdown automata answers,
    \item on subtree pushdown automata that use only one integer as a name of a state instead of a~set of integers, and
    \item to test the size of the representation of a given automaton (not the peak memory consumption of the whole program).
\end{itemize}

%--------------------------------------------------------------------------------------------------
