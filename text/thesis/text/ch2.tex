\chapter{Preliminaries}

This chapter defines the terms and notation used throughout the thesis.
It can be skipped and referred back to it as necessary.
All the definitions are analogous to how they are defined in graph theory and formal languages.
Several works were used as a reference point for this chapter~\cite{Janousek2010-hs, Melichar2005-vj, Sestakova2017-ke, Cormen2009-wz, Cleophas2008-yj, Hopcroft1979-mw}.

%--------------------------------------------------------------------------------------------------
\section{Alphabet, string, language}

An \emph{alphabet} is a finite non-empty set of symbols denoted by $A$.
An alphabet can be \emph{ranked}, which means that each symbol of the alphabet has an associated \emph{arity} with it.
The arity of a~symbol is a non-negative integer denoted by $\operatorname{arity}(a)$.

The symbols of an alphabet are the building blocks of strings.
Formally, a \emph{string} is a finite sequence of symbols over a given alphabet $A$.
The set of all strings over $A$ is denoted by $A^*$, and the set of all non-empty strings over $A$ is denoted by $A^+$.
The \emph{length} of a string is a non-negative integer that represents the number of symbols in a string.
For a string $x$, the length is denoted by $|x|$.
Furthermore, we define a special string, \emph{empty string}, of zero length, denoted by $\varepsilon$.
In the thesis, we use the letters $x$, $y$, and $z$ for strings, and the letters $a$, $b$, and $c$ for symbols of an alphabet.

A \emph{language} $L$ over an alphabet $A$ is a set of strings over $A$, formally $L \subseteq A^*$.
The \emph{size} of a~language is equal to the number of strings in $L$.
A language is \emph{finite} when it contains a finite number of strings.

Throughout this thesis, we refer to various parts of a string.
Let us have a non-empty string $x = a_1 a_2 a_3 \dots a_n$ of length $n$.
A \emph{prefix} of $x$ is any string $y = a_1 a_2 a_3 \dots a_m$ where $1 \leq m \leq n$.
A~\emph{suffix} of $x$ is any string $y = a_{i} a_{i+1} a_{i+2} \dots a_n$ where $1 \leq i \leq n$.
A \emph{factor (substring)} of $x$ is any string where $y = a_{i} a_{i+1} a_{i+2} \dots a_j$ where $1 \leq i \leq j \leq n$.
A \emph{subsequence} of $x$ is any non-empty string that can be obtained by deleting zero or more symbols from $x$.

\begin{example}[Alphabet, string, language]
Let us have an alphabet $A=\{a,b,c\}$ and a string $x=cbbab$.
Then, $|x|=5$. The set of all prefixes is $\{ c, cb, cbb, cbba, cbbab\}$.
The set of all suffixes is $\{ b, ab, bab, bbab, cbbab\}$.
The set of all factors of $x$ includes all its prefixes, suffixes, and more, for example: $b$, $a$, $c$, $cb$, $ba$, $ab$, $bba$, $cbb$, or $bab$.
The set of all subsequences includes all factors and more, for example: $cb$, $ab$, $ca$, $cab$, $cbbb$, or $bbb$.

\end{example}
%--------------------------------------------------------------------------------------------------

%--------------------------------------------------------------------------------------------------
\section{Graph, tree, forest}

In this section, we formally define the notion of a tree. 
We achieve that by using the concepts formulated in graph theory.
First, we review the notion of a graph.
Second, we state the definition of a tree and its properties.
Finally, we review the different types of trees.

A \emph{graph} is a pair $(V, E)$, where $V$ is a set of \emph{vertices} and $E$ is a set of unordered pairs of vertices called \emph{edges}; if $p$ and $q$ are vertices, then $[p,q]$ denotes that there is an edge between $p$ and $q$.
There are two particular parts of a graph that we need to define.
A \emph{path in a graph} is a~sequence of distinct vertices $(v_0, v_1, v_2 \dots v_m)$, where $m \geq 1$.
The path has $m$ edges so that for every $i \in \{1, 2 \dots m\}$ there is an edge $[v_{i-1}, v_i]$.
The \emph{length} of a path is defined by the number of edges on the path.
A \emph{cycle in a graph} is a sequence of vertices $(v_0, v_1, v_2 \dots v_m)$, where $m \geq 3$ and all vertices are distinct except $v_0$ and $v_m$, which are equal.
The cycle has $m$ edges, so for every $ i \in \{1, 2 \dots m\}$ there is an edge $[v_{i-1}, v_i]$.
The \emph{size} of a cycle is the number of vertices (edges) in the cycle.
For example, Figure \ref{img:path-cycle} shows a graph containing a path of length 3 and a~graph containing a cycle of size 3.

\begin{figure}
\centering
\input{diagrams/21}
\caption[Example of a path and a cycle]{On the left is a graph containing a path of length 3 and on the right is a graph containing a cycle of size 3.}
\label{img:path-cycle}
\end{figure}

A graph can have different characteristics. Let us have a graph $G=(V,E)$.
$G$ is \emph{connected} when there is a path between every two vertices in $V$.
$G$ is called \emph{labeled} when it has a mapping from the vertices of $G$ to symbols of an alphabet.
Every vertex $v \in V$ has a property called a~degree.
The \emph{degree} of a vertex $v$ is the number of edges $[v, w] \in E$, where $w \in V$.

The following paragraphs formally define trees in graph theory.
A \emph{tree} is a connected graph without cycles, sometimes also called a \emph{free tree}.
A \emph{forest} is a graph without cycles (a set of trees).
By convention, we call the vertices of a tree \emph{nodes}.
In the following, we define some of the different types of trees.

\begin{definition}[Rooted tree]
Let us have a tree $(V,E)$. A \emph{rooted} tree has one special node $r \in V$ called the \emph{root} of the tree. 
\end{definition}

When a diagram depicts a rooted tree, its root is typically represented by a node at the top of a tree.
For example, in Figure \ref{img:rooted-labeled}, we can see a tree with its root labeled by $a$.

Additionally, we describe various relationships between nodes in rooted trees.
Let $(V,E)$ be a rooted tree with nodes $v, w \in V$.
Node $w$ is a \emph{child} of $v$ and $v$ is the \emph{parent} of $w$ when there is an edge $[v,w]$ between them and the path from the root node to $v$ is shorter than that from the root to $w$.
The nodes $v$ and $w$ are \emph{siblings} when they have the same parent.
The node $w$ is a~\emph{descendant} of $v$ and $v$ is an \emph{ancestor} of $w$ when there exists a path from $v$ to $w$ on a path from the root node to $w$.
Moreover, we define a specific type of node called a leaf.
A \emph{leaf} is a node without children.

A tree has certain attributes that we measure.
A \emph{height} of a tree is the number of edges on the longest path from the root node to a leaf node.
A \emph{size} of a tree is the size of the set $V$. 

\begin{definition}[Labeled tree]
A tree is \emph{labeled} when the underlying graph is labeled.
\end{definition}

\begin{definition}[Ranked tree]
Let us have a ranked alphabet $A$ and a tree $T = (V, E)$.
A \emph{ranked tree} $T$ is a labeled rooted tree, where the labeling is over $A$; and the number of children of every node $v \in V$ with label $a \in A$ is defined by $\operatorname{arity}(a)$. 
\end{definition}

\begin{definition}[Ordered tree]
Let us have a rooted tree $T=(V, E)$.
$T$ is \emph{ordered} when for each node $v \in V$ its children are ordered.
\end{definition}


\begin{example}[Height and size of a tree, degree of a node]
Let us have Figure \ref{img:rooted-labeled} as an example of a rooted labeled tree.
The height of the tree is 2 and its size is 8.
The root of the tree is the node labeled $a$.
The node $a$ has a degree equal to 4 and $b$ has a degree equal to~3.
The nodes $f$ and $g$ are siblings.
The node $b$ is the parent of $g$ and $g$ is a child of $b$.
The node $f$ is a descendant of $a$ and $a$ is an ancestor of $f$.
\end{example}

\begin{figure}
\centering
\input{diagrams/22}
\caption[Rooted labeled tree]{An example of a rooted labeled tree.}
\label{img:rooted-labeled}
\end{figure}

%--------------------------------------------------------------------------------------------------

%--------------------------------------------------------------------------------------------------
\section{Finite and pushdown automata}

One approach to indexing trees, which we discuss in Chapter \ref{chapter:3}, uses automata as its model of computation.
In this section, we define two types of automata.
First, we define a finite automaton (FA) and then a pushdown automaton (PDA).
Both of these are shown in their deterministic and nondeterministic versions.

A \emph{deterministic finite automaton} is a quintuple $(Q,A,\delta, q_0, F)$, where $Q$ is a finite set of states, $A$ is an alphabet, $\delta$ is a mapping from $Q \times A$ to $Q$, elements of which are called \emph{transitions}, $q_0 \in Q$ is an initial state, $F \subseteq Q$ is a set of final states.
In a \emph{nondeterministic finite automaton}, $\delta$ is a~mapping from $Q \times A$ into the power set of $Q$.

We use configurations to describe the steps of computation of both nondeterministic and deterministic finite automata.
Let there be a finite automaton $M=(Q,A,\delta, q_0, F)$.
A \emph{configuration} of $M$ is an ordered pair $(q,x) \in Q \times A^*$.
Let there be a relation over the configurations $\vdash_M \subset (Q \times A^*) \times (Q \times A^*)$.
An element of the relation $\vdash_M$ is called \emph{move}.
If $p \in \delta(q,a)$ or $\delta(q,a)=p$ (in the case of a deterministic FA), then $(q, ax) \vdash_M (p,x)$, where $a \in A $, $x \in A^*$, and $p,q \in Q$.
Furthermore, $\vdash_M^+$ means that there is at least one move between the two configurations, and $\vdash_M^*$ means that there is any number of moves.
A finite automaton \emph{accepts} a string $x$ when $ (q_0, x) \vdash^* (q, \varepsilon)$, where $q \in F$.

The \emph{language} accepted by a finite automaton is the set of strings accepted by the automaton.
Two finite automata are \emph{equivalen}t if they accept the same language.
We can convert every nondeterministic FA into an equivalent deterministic FA.
An example of a deterministic finite automaton is shown in Figure \ref{img:deterministic-FA}.


\begin{figure}
\centering
\input{diagrams/23}
\caption[Deterministic finite automaton]{A deterministic FA $M=(\{a,b\},\{1,0\},\delta, a, \{b\})$, where $\delta$ transitions are $\delta(a,0)=a$, $ \delta(a,1)=b$, $ \delta(b,0)=a$, and $\delta(b,1)=b$. $M$ accepts all odd binary numbers.}
\label{img:deterministic-FA}
\end{figure}

A \emph{nondeterministic pushdown automaton} is a 7-tuple $(Q,A,G,\delta, q_0,Z_0, F)$, where $Q$ is a finite set of states, $A$ is an alphabet, $G$ is a pushdown store alphabet, $\delta$ is a mapping from a finite subset $Q \times (A \cup \{ \varepsilon\}) \times G^*$ into a set of finite subsets $Q \times G^*$ called transitions, $q_0 \in Q$ is an initial state, $Z_0 \in G$ is an initial pushdown store symbol, $F \subseteq Q$ is a set of final states. A \emph{deterministic pushdown automaton} means that the following rules hold for $ \forall q \in Q, \forall a \in ( A \cup \{ \varepsilon\}), \forall \gamma, \beta, \alpha \in G^*$:

\begin{itemize}
    \item $| \delta ( q, a, \gamma)| \leq 1$.
    Informally, for each starting point of a transition, there is at most one possible outcome.
    \item If $\delta ( q, a, \alpha ) \neq \emptyset$, $\delta ( q, a, \beta ) \neq \emptyset$ and $\alpha \neq \beta$, then $\alpha$ is not a prefix of $\beta$ and vice versa.
    Informally, when there are two transitions from the same state on the same symbol on the input, then they have to differ in what they expect on the pushdown store.
    \item If $\delta ( q, a, \alpha ) \neq \emptyset$, $\delta ( q, \varepsilon, \beta ) \neq \emptyset$, then $\alpha$ is not a prefix of $\beta$ and vice versa.
    Informally, when there are two transitions from the same state, and at least one does not accept anything from the input, they have to be completely different in what they expect on the pushdown store.
\end{itemize}

To describe the computation steps of a PDA, we also use configurations.
Let there be a PDA $M=(Q,A,G,\delta, q_0,Z_0 F)$.
A \emph{configuration} of $M$ is an ordered triple $(q,x, \gamma) \in Q \times A^* \times G^*$.
Let there be a relation over the configurations $\vdash_M \subset (Q \times A^*\times G^*) \times (Q \times A^*\times G^*)$. If $(p,\beta) \in \delta(q,a,\alpha )$, then $(q, ax, \alpha \gamma) \vdash_M (p,x, \beta \gamma)$, where $a \in A$, $x \in A^*$, $\alpha, \beta, \gamma \in G^*$, and $p,q \in Q$.
Furthermore, same as with finite automata, $\vdash_M^+$ means that there is at least one move between the two configurations and $\vdash_M^*$ means that there is any number of moves.

The \emph{language} accepted by a pushdown automaton $M$ is the set of strings accepted by the automaton, denoted by $L(M)$.
Similar to finite automata, two pushdown automata are \emph{equivalent} when they accept the same language.

In contrast to finite automata, pushdown automata have two ways of accepting a string.
In this thesis, we only use pushdown automata that accept a language by having an empty pushdown store. That is, a PDA $M=(Q,A,G,\delta, q_0,Z_0, F)$ accepts a language when:

$$ L(M) = \{ x: x \in A^*, \exists q \in Q, (q_0, x, Z_0) \vdash^* (q, \varepsilon, \varepsilon )  \}.$$

By convention, to signify that a PDA accepts by an empty pushdown store, we denote the set of final states by the empty set $\emptyset$.

In contrast to finite automata, not every nondeterministic PDA has an equivalent deterministic PDA that accepts the same language.
Therefore, we recognize the types of pushdown automata that have the ability, which includes input-driven pushdown automata.
An \emph{input-driven} PDA determines each pushdown operation only by the symbol on the input \cite{Janousek2010-hs}.
Furthermore, a~PDA is \emph{acyclic} if it does not contain transitions that would allow visiting one state, accepting some symbols on the input, and revisiting the same state \cite{Travnicek2012-mz}. 

In diagrams, a transition labeled $a, \gamma/\alpha$ from a state $q$ to a state $p$ means that $(p, \alpha) \in \delta(q, a, \gamma)$, where $a \in A \cup \{ \varepsilon \}$ is what we accept on the input, $\gamma \in G^*$ is the content at the top of the pushdown store that is erased, and $\beta \in G^*$ is what is written at the top of the pushdown store after the move.
In Figure \ref{img:deterministic-PDA}, we see an example of a deterministic PDA and its diagram.

\begin{figure}[H]
\centering
\input{diagrams/24}
\caption[Deterministic pushdown automaton]{A deterministic PDA $M=(\{a,b\},\{0,1\},\{A,Z\},\delta, a,Z, \emptyset)$. $M$ accepts a string by an empty pushdown store and accepts a language, where every string is in the form $0^n 1^n$.}
\label{img:deterministic-PDA}
\end{figure}


%--------------------------------------------------------------------------------------------------
