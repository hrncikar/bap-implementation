\chapter{Introduction}
\label{chapter:1}

When working with hierarchical data, it is often convenient to use a tree structure to represent them.
Searching the data for a specific class of patterns is called the \emph{tree indexing problem}.
The aim of tree indexing problem solutions is to preprocess a given input tree so that we can find all occurrences of query patterns within the tree in the shortest amount of time possible. 
Many areas rely on such tree indexing algorithms.
These areas are, for example, disciplines in bioinformatics such as phylogeny or work with DNA and RNA structures \cite{Chi2003-ny}, or in computer science, compiler code optimizations \cite{Flouri2011-dc}, file systems, and XML query languages.

Due to the varied applications of tree indexes, there are many tree indexing problems, each with nuances that match the intended use.
The aim of this thesis is to explore the different variants of tree indexing problems and categorize them.
We describe four properties that can be used to define a tree indexing problem.
The properties are:
\begin{itemize}
    \item the type of trees we index,
    \item the type of patterns we are looking for,
    \item what we consider to be a match, and
    \item the type of answers we require.
\end{itemize}
Tree indexing is also a problem that can be quite easily expanded to other tasks.
These tasks can be considered more complex since the focus is not on answering a simple occurrence question but rather on drawing some conclusions based on the structure of the data.
For example, many publications focus on finding a common pattern in a vast dataset.
The problem is often called \emph{searching for tree repetitions} \cite{Flouri2011-dc, Asai2003-ao}, or \emph{tree mining problem} \cite{Chi2003-ny}.

%--------------------------------------------------------------------------------------------------
\section{Aim of the thesis}
The aim of this thesis is to research tree indexing problems.
The study consists of three main parts.
First, we aim to survey tree indexing problems and definitions used in the literature.
Second, our aim is to choose and study one of the problems described in the first part of this thesis in more depth.
We mean to identify possible approaches to the problem and compare and contrast some of its solutions.
Our final aim is to implement two solutions to the subtree rejection problem and to compare their efficiency.
For that, we must prepare data and experiments that measure time and memory consumption.

%--------------------------------------------------------------------------------------------------

%--------------------------------------------------------------------------------------------------
\section{Structure of the thesis}

The rest of the thesis is organized as follows.
The second chapter lays the foundation for the thesis by defining the necessary terms used in this work. Namely, we present basic definitions from graph theory such as graph, tree, and forest.
Then we define concepts from automata theory, specifically pushdown and finite automata.
The third chapter shows the results of our research on tree indexing problems.
The fourth chapter explains and compares three solutions to the subtree rejection problem, specifically the subtree pushdown automata, the subtree oracle pushdown automata, and the subtree finite automata.
The fifth chapter presents the implementations of the three solutions.
The sixth chapter compares the performance of the three implementations.
The last chapter summarizes the most important results of the thesis.

%--------------------------------------------------------------------------------------------------