#!/bin/sh

make clean
make

results=data/experiment/results
counter=1

timeTest ()
{
    queries=$1
    trees=$2

    for f in "$trees"/* 
    do
        ./bap subtreeOraclePDA "$f" "$queries" "$results"/logOraclePDA.txt  > "$results"/tmpO.txt
        ./bap subtreePDA       "$f" "$queries" "$results"/logSubtreePDA.txt > "$results"/tmpS.txt
        ./bap subtreeFA        "$f" "$queries" "$results"/logSubtreeFA.txt  > "$results"/tmpF.txt

        cat "$results"/tmpO.txt >> "$results"/queriesOraclePDA.txt  # to log the query answers
        cat "$results"/tmpS.txt >> "$results"/queriesSubtreePDA.txt # to log the query answers
        cat "$results"/tmpF.txt >> "$results"/queriesSubtreeFA.txt  # to log the query answers

        diff -y --suppress-common-lines "$results"/tmpO.txt "$results"/tmpS.txt | wc -l >> "$results"/falsePositives.txt

        echo "TIME: Tree $counter done"
        counter=$(($counter + 1))
    done
}

timeTest data/experiment/dataset/queries/queriesD01.txt data/experiment/dataset/trees/treesD0001 

timeTest data/experiment/dataset/queries/queriesD02.txt data/experiment/dataset/trees/treesD0003

timeTest data/experiment/dataset/queries/queriesD03.txt data/experiment/dataset/trees/treesD0007

timeTest data/experiment/dataset/queries/queriesD04.txt data/experiment/dataset/trees/treesD0011

timeTest data/experiment/dataset/queries/queriesD06.txt data/experiment/dataset/trees/treesD0015

make clean

rm "$results"/tmpO.txt
rm "$results"/tmpS.txt
rm "$results"/tmpF.txt
