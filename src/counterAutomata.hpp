#ifndef BAP_COUNTERAUTOMATA_HPP
#define BAP_COUNTERAUTOMATA_HPP

#include <map>
#include <tuple>
#include <set>

#include "subtreeRejectionIndex.hpp"
#include "state.hpp"

#define Z_0  1     // there is always only one initial pushdown store symbol
#define PARENTS 1   

//=================================================================================================
/*
  This abstract class represents a fast rejection index realised by counter automata.
  That is a special type of pushdown automata that needs only counter instead of pushdown store.
  While this class can represent a nondeterministic automaton, running a query when the automaton is
  nondeterministic can return a wrong false. Therefore, an automaton should be run only once 
  it has been made deterministic.

  This automaton represent only pushdown automata accepting by an empty pushdown store.
*/

class CCounterAutomata : public CSubtreeRejectionIndex
{
  public:
    virtual                           ~CCounterAutomata    ( void ) = default;
    /**
     * Query runs the automaton on a queryPattern and returns whether the automaton accepts the
     * word.
     * @param queryPattern is a string that uses the alphabet symbols
     * @return true when the queryPattern represent a subtree of the subject tree
     * @return false otherwise
     */
    virtual bool                       Query               ( const std::string & queryPattern ) override;
    virtual void                       LogInformation      ( void ) override;
    /**
     * The method makes an input-driven acyclic automaton deterministic according as it is done in
     * the papers.
     */
     void                              Determinize         ( void );
  protected:
    //---------------------------------------------------------------------------------------------
    /*
      Structures TFrom and TTo together are used to represent a transition delta(TFrom) = {TTo};
    */
    struct TFrom
    {
                                       TFrom               ( void ) = default;
                                       TFrom               ( const CState   &  state,
                                                             int               inputSymbol,
                                                             int               topOfStack );
      bool                             operator <          ( const TFrom     & other ) const;

      CState                           m_State;
      int                              m_InputSymbol;
      int                              m_TopOfStack;
    };
    struct TTo
    {
                                       TTo                 ( void ) = default;
                                       TTo                 ( const CState    & state,
                                                             int               topOfStack );
      bool                             operator <          ( const TTo       & other ) const;

      CState                           m_State;
      int                              m_TopOfStack;
    };
    //---------------------------------------------------------------------------------------------
    /**
     * Deletes the inaccessible transitions and states of this automaton after determinisation
     * @param sc represents all the possible stack contents of each state of the automaton
     */
    void                               deleteInaccessible  ( std::map<CState,std::set<int>> & sc );
    /**
     * The method executes a transition of the automaton.
     * @param from is the input into delta (all the transition rules)
     * @param state is set to the current state if the move was successful
     * @param stackCounter is the number of symbols in the pushdown store
     * @return true is the transition was possible to execute (correct top of the pushown store)
     *         otherwise false 
     */
    bool                               tryMove             ( std::map<TFrom, std::set<TTo>>::iterator from,
                                                             CState          & state, 
                                                             int             & stackCounter );
    /**
     * Deletes all the content of the automaton.
     */
    void                               clear               ( void );
    /**
     * Deletes outgoing transitions of a given state.
     * @param state whose outgoing transitions are to be deleted
     */
    void                               deleteOutgoing      ( const CState    & state );
    /**
     * Deletes ingoing transitions of a given state.
     * @param state whose ingoing transitions are to be deleted
     */
    void                               deleteIngoing       ( const CState    & state );
    /**
     * Copies ingoing transitions of one state onto another.
     * @param from state is the state whose ingoing transitions we copy
     * @param to state is the state onto which we copy them
     */
    void                               copyIngoing         ( const CState    & from,
                                                             const CState    & to );
    /**
     * Copies outgoing transitions of one state onto another.
     * @param from state is the state whose outgoing transitions we copy
     * @param to state is the state onto which we copy them
     */
    void                               copyOutgoing        ( const CState    & from,
                                                             const CState    & to );

    std::set<CState>                   m_Q;
    std::set<int>                      m_A;
    std::map<TFrom, std::set<TTo>>     m_Delta;
    CState                             m_q0;
};
//=================================================================================================
#endif //BAP_COUNTERAUTOMATA_HPP