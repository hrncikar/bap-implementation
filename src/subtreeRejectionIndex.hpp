#ifndef BAP_TREEINDEX_HPP
#define BAP_TREEINDEX_HPP

#include <string>

//=================================================================================================
/**
  An abstract class that defines the required behaviour of a fast rejection tree index.
*/

class CSubtreeRejectionIndex
{
  public:
    virtual                           ~CSubtreeRejectionIndex ( void ) = default;
    /**
     * BuildIndex is an abstrast method that creates a fast rejection index for a given tree.
     * Implementations must ensure that the method can be called many times over.
     * @param[in] tree is the linear notation of a ranked tree for which the index is build
     *            the linear notation is consistent with the one introduced in arbology
     */
    virtual void                       BuildIndex          ( const std::string & tree ) = 0;
    /**
     * Query is an abstract method that returns whether the query was accepted as present in the 
     * subject tree by the index.
     * @param[in] queryPattern is a string that can (but does not have to) represent a tree in 
     *            its linear notation
     * @return true when the queryPattern is accepted by the index
     */
    virtual bool                       Query               ( const std::string & queryPattern ) = 0;
    /**
     * LogInformation is a virtual method which allows to write relevant information about the
     * index into a log file.
     */
    virtual void                       LogInformation      ( void )
    {}
};
//=================================================================================================
#endif //BAP_TREEINDEX_HPP