#ifndef BAP_ORACLEPDA_HPP
#define BAP_ORACLEPDA_HPP

#include <sstream>

#include "subtreePDA.hpp"

//=================================================================================================
/**
 * COraclePDA represents a subtree oracle pushdown automaton. It implements the merging of
 * corresponding states according to the definition introduced in the thesis (not the paper).
 */
class COraclePDA : public CSubtreePDA
{
  public:
    virtual                           ~COraclePDA          ( void ) = default;
    /**
     * BuildIndex builds deterministic subtree oracle pushdown automaton. 
     * First it creates the subtree pushdown automaton and then it merges its corresponding states.
     * It also renames the states into sets of size equal to one.
     * @param[in] tree is the linear notation of a ranked tree for which the index is build
     */
    virtual void                       BuildIndex          ( const std::string & tree ) override;
  private:
    /**
     * MergeStates merges any two states into one (new) state and deletes the old transitions.
     * Note that it does not erase any states.
     * @param[in] one is the first state to merge
     * @param[in] two is the second state to merge
     * @return a state created by merging the states from parameters
     */
    CState                             mergeStates         ( const CState    & one, 
                                                             const CState    & two );
    /**
     * The method transfers transitions from one state into another.
     * It deletes the transition of the states from which we transfer.
     * @param[in] oldsy is the state whose transitions we copy onto the other state
     * @param[in] newsy is the state to which we add the transitions
     */                                                        
    void                               transferTransitions ( const CState    & oldsy, 
                                                             const CState    & newsy );
};
//=================================================================================================
#endif //BAP_ORACLEPDA_HPP