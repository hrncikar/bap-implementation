#ifndef BAP_LOG_HPP
#define BAP_LOG_HPP

#include <iostream>

//=================================================================================================
class CLog 
{
  public:
    static void                        Init                ( std::ostream    & out )
    {
      s_Out = &out;
    }

    template<typename ... TArgs >
    static void                        Log                 ( const char      * format,
                                                             const TArgs & ... args )
    {
      print ( format, args ... );
    }

  private:
    static std::ostream              * s_Out;

    static void                        print               ( const char      * format )
    {
      *s_Out << format;
      s_Out -> flush ();
    }

    template< typename T, typename ... TArgs >
    static void                        print               ( const char      * format,
                                                             const T         & value, 
                                                             const TArgs & ... args )
    {
      for ( ; *format ; format ++ )
      {
        if ( *format == '%'  )
        {
          *s_Out << value;
          print ( format + 1, args ... );
          return;
        }
        *s_Out << *format;
      }
    }
};

#ifdef LOG_ENABLE
  #define LOG(...)          ::CLog::Log ( __VA_ARGS__ )
#else
  #define LOG(...)
#endif

//=================================================================================================
#endif //BAP_LOG_HPP
