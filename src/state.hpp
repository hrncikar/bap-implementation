#ifndef BAP_STATE_HPP
#define BAP_STATE_HPP

#include <set>

//=================================================================================================
/**
 * CState represents a state of an automaton.
 */
class CState
{
  public:
                                       CState              ( void ) = default;
                                       CState              ( const CState    & copy );
                                       CState              ( const std::set<int>  & copy );
                                      ~CState              ( void ) = default; 
    bool                               operator <          ( const CState    & other ) const;
    bool                               operator ==         ( const CState    & other ) const;
    CState                           & operator =          ( const CState    & copy ) = default;
    const std::set<int>              & Name                ( void ) const;
    /**
     * Adds an integer to the set that represent the label of the state.
     * @param addName is the integer to add into the set of labels
     * @return std::set<int>&  that represent the name of the state
     */
    std::set<int>                    & Insert              ( int               addName );
    bool                               Empty               ( void ) const;
    size_t                             Size                ( void ) const;
    /**
     * The methods returns whether this state is corresponding to another.
     * @param other is the state we test for corresponding
     * @return true when this state is corresponding to the other state
     * @return false otherwise
     */
    bool                               AreCorresponding    ( const CState    & other ) const;
  
  private:
    std::set<int>                      m_Label;
};
//=================================================================================================
#endif //BAP_STATE_HPP