#include <iostream>

#include "subtreeFA.hpp"

using namespace std;

//=================================================================================================
void               CSubtreeFA::BuildIndex                  ( const std::string & tree )
{
  clear ();
  set<vector<int>> subtrees = subtreeSet ( tree );

  int counter = 0;

  CState q;
  m_q0 = CState ( {counter} );
  m_Q . insert ( m_q0 );

  for ( const auto & subtree : subtrees )
  {
    q = m_q0;
    for ( auto label : subtree )
    {
      TFrom from ( q, label );
      auto x = m_Delta . find ( from );

      if ( x != m_Delta . end () )
        q = x -> second . m_State;
      else
      {
        q = CState ( {++counter} );
        m_Q . insert ( q );
        m_Delta[from] = q;
      }
    }
    m_F . insert ( q );
  }
}
//-------------------------------------------------------------------------------------------------
set<vector<int>>   CSubtreeFA::subtreeSet                  ( const std::string & tree )
{
  set<vector<int>> result;

  int label;
  vector<int> t;

  stringstream iss ( tree );
  while ( iss >> label )
    t . push_back ( label );
  
  for ( size_t i = 0; i < t . size (); ++i )
  {
    if ( t[i] == 0 )
    {
      result . insert ( {0} );
      continue;
    }

    int ac = t[i];
    int n = 1;
    size_t j = 1;

    vector<int> subtree;
    subtree . push_back ( t[i] );

    while ( ac - n + 1 != 0 && j < t . size () )
    {
      ac += t[i+j];
      subtree . push_back ( t[i+j] );
      ++j; 
      ++n;
    }
    result . insert ( subtree );

  }
  return result;
}
//=================================================================================================