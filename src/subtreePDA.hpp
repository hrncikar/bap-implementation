#ifndef BAP_SUBTREEPDA_HPP
#define BAP_SUBTREEPDA_HPP

#include <sstream>

#include "counterAutomata.hpp"

//=================================================================================================
/**
 * CSubtreePDA class represent a subtree pushdown automaton. Before using it it has to be built by
 * BuildIndex(tree).
 * The class labels the automatons' states consistently with the methods in the paper. Therefore,
 * theoretically a new query method could extract the location of the potential subtree. However,
 * for our purposes of a fast rejection index it is unnecessary.
 */
class CSubtreePDA : public CCounterAutomata
{
  public:
    virtual                           ~CSubtreePDA         ( void ) = default;
    /**
     * BuildIndex creates a deterministic subtree pushdown automaton for a given tree.
     * @param[in] tree is the linear notation of a ranked tree for which the index is build
     */
    virtual void                       BuildIndex          ( const std::string & tree ) override;
};
//=================================================================================================
#endif //BAP_SUBTREEPDA_HPP