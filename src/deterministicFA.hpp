#ifndef BAP_FINITEAUTOMATA_HPP
#define BAP_FINITEAUTOMATA_HPP

#include <map>
#include <set>

#include "subtreeRejectionIndex.hpp"
#include "state.hpp"
#include "log.hpp"

//=================================================================================================
/**
 * An abstract class that represents deterministic pushdown automata.
 * 
 */
class CDeterministicFA : public CSubtreeRejectionIndex
{
  public:
    virtual                           ~CDeterministicFA    ( void ) = default;
    /**
     * Query runs the automaton on the queryPatterns and returns whether the automaton accepts the
     * language.
     * @param queryPattern is a string that uses the alphabet symbols
     * @return true when the queryPattern represent a subtree of the subject tree
     * @return false otherwise
     */
    virtual bool                       Query               ( const std::string & queryPattern ) override;
    virtual void                       LogInformation      ( void ) override;
  
  protected:
    //---------------------------------------------------------------------------------------------
    /*
      Structures TFrom and TTo together are used to represent a transition delta(TFrom) = {TTo};
    */
    struct TFrom
    {
                                       TFrom               ( void ) = default;
                                       TFrom               ( const CState   &  state,
                                                             int               inputSymbol );
      bool                             operator <          ( const TFrom     & other ) const;

      CState                           m_State;
      int                              m_InputSymbol;
    };
    struct TTo
    {
                                       TTo                 ( void ) = default;
                                       TTo                 ( const CState    & state );
      bool                             operator <          ( const TTo       & other ) const;

      CState                           m_State;
    };
    //---------------------------------------------------------------------------------------------
    void                               clear               ( void );
    
    std::set<CState>                   m_Q;
    std::map<TFrom, TTo>               m_Delta;
    CState                             m_q0;
    std::set<CState>                   m_F;
};
//=================================================================================================
#endif //BAP_FINITEAUTOMATA_HPP