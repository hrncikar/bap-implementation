#include <sstream>
#include <iostream>
#include <assert.h>


#include "oraclePDA.hpp"

using namespace std;

void               COraclePDA::BuildIndex                  ( const std::string & tree )
{
  clear ();

  // get subtree
  CSubtreePDA::BuildIndex ( tree );
  set<CState> newStates;

  // merge corresponding states
  for ( auto it1 = m_Q . begin (); it1 != m_Q . end (); )
  {
    bool isMerged = false;
    auto it2 = it1;
    for ( ++ it2; it2 != m_Q . end (); )
    {
      if ( it1 -> AreCorresponding ( * it2 ) )
      {
        newStates . insert ( mergeStates ( * it1, * it2 ) );

        it2 = m_Q . erase ( it2 );     // one state can merge onto more states, therefore we need to go through the rest

        isMerged = true;
      }
      else
        ++ it2;
    }
    if ( ! isMerged )
    {
      if ( it1 -> Size () == 1 )
      {
        newStates . insert ( *it1 );
        ++ it1;
      }
      else                             // renaming so that determinisation is okay
      {
        newStates . insert ( mergeStates ( *it1,  CState( {* ( it1 -> Name () . begin () ) })));
        it1 = m_Q . erase ( it1 );
      }
    }
    else
      it1 = m_Q . erase ( it1 );
  }

  swap ( m_Q, newStates );
  Determinize ();
}
//-------------------------------------------------------------------------------------------------
CState             COraclePDA::mergeStates                 ( const CState    & one, 
                                                             const CState    & two )
{
  if ( one . Size () == 1 )
  {
    transferTransitions ( two, one );
    return one;
  }
  if ( two . Size () == 1 )
  {
    transferTransitions ( one, two );
    return two;
  }
  CState newState ( { *(one . Name () . begin ()) });
  transferTransitions ( two, newState );
  transferTransitions ( one, newState );
  return newState;
}
//-------------------------------------------------------------------------------------------------
void               COraclePDA::transferTransitions         ( const CState & oldsy, 
                                                             const CState & newsy )
{
  copyIngoing ( oldsy, newsy );
  deleteIngoing ( oldsy );
  copyOutgoing ( oldsy, newsy );
  deleteOutgoing ( oldsy );
}
//=================================================================================================