  CSubtreeFA i;
  i. BuildIndex ( "2 1 0 0"s );
  assert ( i. Query ( "2 1 0 0" ) == true );
  assert ( i. Query ( " 1 0" ) == true );
  assert ( i. Query ( " 0" ) == true );
  assert ( i. Query ( " 0 0" ) == false );
  assert ( i. Query ( "2 1 0 " ) == false );
  cout << "Tree \"2 1 0 0\" ok" << endl;

  i. BuildIndex ( "2 0 0"s );
  assert ( i. Query ( "2 0 0"  ) == true );
  assert ( i. Query ( "0"  ) == true );
  assert ( i. Query ( "0 0 0"  ) == false );
  assert ( i. Query ( "0 0" ) == false );
  assert ( i. Query ( "2 2 2" ) == false );
  assert ( i. Query ( "1" ) == false );
  assert ( i. Query ( "1 2 3" ) == false );
  cout << "Tree \"2 0 0\" ok" << endl;
  
  i. BuildIndex ( "3 0 2 0 0 0"s );
  assert ( i. Query (  "3 0 2 0 0 0"  ) == true );
  assert ( i. Query (  "2 0 0" ) == true );
  assert ( i. Query (  "0" ) == true );
  assert ( i. Query (  "2 0 0 0" ) == false );
  assert ( i. Query (  "3 2 2 0 0 0" )  == false );
  assert ( i. Query (  "3 0 2 0 0 0 0"  ) == false );
  assert ( i. Query (  "0 0" )  == false );
  cout << "Tree \"3 0 2 0 0 0\" ok " << endl;

  i. BuildIndex ( "1 1 1 1 1 0"s );
  assert ( i. Query ( "1 1 1 1 1 0"  ) == true );
  assert ( i. Query ( "0" )  == true );
  assert ( i. Query ( "1 0"  ) == true );
  assert ( i. Query ( "1 1 1 0"  ) == true );
  assert ( i. Query ( "1" )  == false );
  assert ( i. Query ( "1 1"  ) == false );
  assert ( i. Query ( "1 1 1 1 1 1 0"  ) == false );
  cout << "Tree \"1 1 1 1 1 0\" ok " << endl;

  i. BuildIndex ( "6 0 0 0 0 0 0"s );
  assert ( i. Query ( "6 0 0 0 0 0 0" ) == true );
  assert ( i. Query ( "0" ) == true );
  assert ( i. Query ( "6 "  ) == false );
  assert ( i. Query ( "0 0" ) == false );
  assert ( i. Query ( "6 0 0 0 0 0 0 0 " ) == false );
  assert ( i. Query ( "6 0 0 0 " ) == false );
  cout << "Tree \"6 0 0 0 0 0 0\" ok " << endl;


  i. BuildIndex ( "0"s );
  assert ( i. Query ( "0" ) == true );
  assert ( i. Query ( "0 0" ) == false );
  cout << "Tree \"0\" ok " << endl;

  i. BuildIndex ( "2 2 0 0 1 0"s );
  assert ( i. Query ( "2 2 0 0 1 0"  ) == true );
  assert ( i. Query ( "2 0 0"  ) == true );
  assert ( i. Query ( "1 0"  ) == true );
  assert ( i. Query ( "0"  ) == true );
  assert ( i. Query ( "0 0" ) == false );
  assert ( i. Query ( "2 0 0 1 0"  ) == false );
  assert ( i. Query ( "2 0 0 1" ) == false );
  cout << "Tree \"2 2 0 0 1 0\" ok " << endl;

  i. BuildIndex ( "2 2 0 1 0 1 0"s );
  assert ( i. Query ( "2 2 0 1 0 1 0"  ) == true );
  assert ( i. Query ( "2 0 1 0" )  == true );
  assert ( i. Query ( "1 0" )  == true );
  assert ( i. Query ( "0" )  == true );
  assert ( i. Query ( "0 0"  ) == false );
  assert ( i. Query ( "2 0 0 1 0"  ) == false );
  assert ( i. Query ( "2 0 0 1"  ) == false );
  assert ( i. Query ( "2 0 1 0 1 0" ) == false );
  assert ( i. Query ( "2 0 0" ) == false );
  assert ( i. Query ( "2 0 0 1" ) == false );
  cout << "Tree \"2 2 0 1 0 1 0\" ok " << endl;

  i. BuildIndex ( "2 2 0 0 0"s );
  assert ( i. Query ( "2 2 0 0 0"  ) == true );
  assert ( i. Query ( "2 0 0" )  == true );
  assert ( i. Query ( "0" )  == true );
  assert ( i. Query ( "2 0 0 0"  ) == false );
  assert ( i. Query ( "2 2 0 0 0 0" ) == false );
  cout << "Tree \"2 2 0 0 1 0\" ok " << endl;

  i. BuildIndex ( "3 2 0 0 0 1 0"s );
  assert ( i. Query ( "3 2 0 0 0 1 0" ) == true );
  assert ( i. Query ( "2 0 0" ) == true );
  assert ( i. Query ( "1 0" )  == true );
  assert ( i. Query ( "0" )  == true );
  assert ( i. Query ( "0 0"  ) == false );
  assert ( i. Query ( "2 0 0 1 0"  ) == false );
  assert ( i. Query ( "3 0 0 0" ) == false );
  assert ( i. Query ( "3 0 0 1 0" ) == false );
  cout << "Tree \"3 2 0 0 0 1 0\" ok " << endl;

  i. BuildIndex ( "2 0 2 0 2 0 0"s );
  assert ( i. Query ( "2 0 2 0 2 0 0"  ) == true );
  assert ( i. Query ( "2 0 2 0 0" )  == true );
  assert ( i. Query ( "2 0 0" )  == true );
  assert ( i. Query ( "0" )  == true );
  assert ( i. Query ( "2 2 0"  ) == false );
  assert ( i. Query ( "2 2 0 0"  ) == false );
  assert ( i. Query ( "2 2 0 0 0"  ) == false );
  assert ( i. Query ( "2 2 0 0 0 0"  ) == false );
  assert ( i. Query ( "2 2 2 0"  ) == false );
  assert ( i. Query ( "2 2 2 0 0"  ) == false );
  assert ( i. Query ( "2 2 2 0 0 0"  ) == false );
  assert ( i. Query ( "2 2 2 0 0 0 0" ) == false );
  cout << "Tree \"2 0 2 0 2 0 0\" ok " << endl;

  i. BuildIndex ( "2 2 2 0 2 0 1 0 0 0"s );
  assert ( i. Query ( "2 2 2 0 2 0 1 0 0 0"  ) == true );
  assert ( i. Query ( "2 2 0 2 0 1 0 0 "  ) == true );
  assert ( i. Query ( "2 0 2 0 1 0" ) == true );
  assert ( i. Query ( "2 0 1 0" ) == true );
  assert ( i. Query ( "0" ) == true );
  assert ( i. Query ( "1 0" ) == true );
  assert ( i. Query ( "2 0 0" ) == false );
  assert ( i. Query ( "2 0 2 0 0" ) == false );
  assert ( i. Query ( "2 2 2  0" ) == false );
  assert ( i. Query ( "2 2 0 2 0 0 0 " ) == false );
  assert ( i. Query ( "2 2 2 0 1 0 0 0" ) == false );
  assert ( i. Query ( "2 2 0 1 0 0" ) == false );
  assert ( i. Query ( "2 2 2 0 2  0 0" ) == false );
  assert ( i. Query ( "2 2 2 0 0 0" ) == false );
  assert ( i. Query ( "2  2 2 0 0 0" ) == false );
  cout << "Tree \"2 2 2 0 2 0 1 0 0 0\" ok " << endl;

  cout << "SubtreeFA ok " << endl;