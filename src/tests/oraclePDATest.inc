  COraclePDA o ;
  o . BuildIndex ( "2 1 0 0"s );
  assert ( o . Query ( "2 1 0 0" ) == true );
  assert ( o . Query ( " 1 0" ) == true );
  assert ( o . Query ( " 0" ) == true );
  assert ( o . Query ( " 0 0" ) == false );
  assert ( o . Query ( "2 1 0 " ) == false );
  cout << "Tree \"2 1 0 0\" ok" << endl;

  o . BuildIndex ( "2 0 0"s );
  assert ( o . Query ( "2 0 0"  ) == true );
  assert ( o . Query ( "0"  ) == true );
  assert ( o . Query ( "0 0 0"  ) == false );
  assert ( o . Query ( "0 0" ) == false );
  assert ( o . Query ( "2 2 2" ) == false );
  assert ( o . Query ( "1" ) == false );
  assert ( o . Query ( "1 2 3" ) == false );
  cout << "Tree \"2 0 0\" ok" << endl;
  
  o . BuildIndex ( "3 0 2 0 0 0"s );
  assert ( o . Query (  "3 0 2 0 0 0"  ) == true );
  assert ( o . Query (  "2 0 0" ) == true );
  assert ( o . Query (  "0" ) == true );
  assert ( o . Query (  "2 0 0 0" ) == false );
  assert ( o . Query (  "3 2 2 0 0 0" )  == false );
  assert ( o . Query (  "3 0 2 0 0 0 0"  ) == false );
  assert ( o . Query (  "0 0" )  == false );
  cout << "Tree \"3 0 2 0 0 0\" ok " << endl;

  o . BuildIndex ( "1 1 1 1 1 0"s );
  assert ( o . Query ( "1 1 1 1 1 0"  ) == true );
  assert ( o . Query ( "0" )  == true );
  assert ( o . Query ( "1 0"  ) == true );
  assert ( o . Query ( "1 1 1 0"  ) == true );
  assert ( o . Query ( "1" )  == false );
  assert ( o . Query ( "1 1"  ) == false );
  assert ( o . Query ( "1 1 1 1 1 1 0"  ) == false );
  cout << "Tree \"1 1 1 1 1 0\" ok " << endl;

  o . BuildIndex ( "6 0 0 0 0 0 0"s );
  assert ( o . Query ( "6 0 0 0 0 0 0" ) == true );
  assert ( o . Query ( "0" ) == true );
  assert ( o . Query ( "6 "  ) == false );
  assert ( o . Query ( "0 0" ) == false );
  assert ( o . Query ( "6 0 0 0 0 0 0 0 " ) == false );
  assert ( o . Query ( "6 0 0 0 " ) == false );
  cout << "Tree \"6 0 0 0 0 0 0\" ok " << endl;


  o . BuildIndex ( "0"s );
  assert ( o . Query ( "0" ) == true );
  assert ( o . Query ( "0 0" ) == false );
  cout << "Tree \"0\" ok " << endl;

  o . BuildIndex ( "2 2 0 0 1 0"s );
  assert ( o . Query ( "2 2 0 0 1 0"  ) == true );
  assert ( o . Query ( "2 0 0"  ) == true );
  assert ( o . Query ( "1 0"  ) == true );
  assert ( o . Query ( "0"  ) == true );
  assert ( o . Query ( "0 0" ) == false );
  assert ( o . Query ( "2 0 0 1 0"  ) == false );
  assert ( o . Query ( "2 0 0 1" ) == false );
  cout << "Tree \"2 2 0 0 1 0\" ok " << endl;

  o . BuildIndex ( "2 2 0 1 0 1 0"s );
  assert ( o . Query ( "2 2 0 1 0 1 0"  ) == true );
  assert ( o . Query ( "2 0 1 0" )  == true );
  assert ( o . Query ( "1 0" )  == true );
  assert ( o . Query ( "0" )  == true );
  assert ( o . Query ( "0 0"  ) == false );
  assert ( o . Query ( "2 0 0 1 0"  ) == false );
  assert ( o . Query ( "2 0 0 1"  ) == false );
  assert ( o . Query ( "2 0 1 0 1 0" ) == false );
  assert ( o . Query ( "2 0 0" ) == false );
  assert ( o . Query ( "2 0 0 1" ) == false );
  cout << "Tree \"2 2 0 1 0 1 0\" ok " << endl;

  o . BuildIndex ( "2 2 0 0 0"s );
  assert ( o . Query ( "2 2 0 0 0"  ) == true );
  assert ( o . Query ( "2 0 0" )  == true );
  assert ( o . Query ( "0" )  == true );
  assert ( o . Query ( "2 0 0 0"  ) == false );
  assert ( o . Query ( "2 2 0 0 0 0" ) == false );
  cout << "Tree \"2 2 0 0 1 0\" ok " << endl;

  o . BuildIndex ( "3 2 0 0 0 1 0"s );
  assert ( o . Query ( "3 2 0 0 0 1 0" ) == true );
  assert ( o . Query ( "2 0 0" ) == true );
  assert ( o . Query ( "1 0" )  == true );
  assert ( o . Query ( "0" )  == true );
  assert ( o . Query ( "0 0"  ) == false );
  assert ( o . Query ( "2 0 0 1 0"  ) == false );
  assert ( o . Query ( "3 0 0 0" ) == false );
  assert ( o . Query ( "3 0 0 1 0" ) == false );
  cout << "Tree \"3 2 0 0 0 1 0\" ok " << endl;

  o . BuildIndex ( "2 0 2 0 2 0 0"s );
  assert ( o . Query ( "2 0 2 0 2 0 0"  ) == true );
  assert ( o . Query ( "2 0 2 0 0" )  == true );
  assert ( o . Query ( "2 0 0" )  == true );
  assert ( o . Query ( "0" )  == true );
  assert ( o . Query ( "2 2 0"  ) == false );
  assert ( o . Query ( "2 2 0 0"  ) == false );
  assert ( o . Query ( "2 2 0 0 0"  ) == false );
  assert ( o . Query ( "2 2 0 0 0 0"  ) == false );
  assert ( o . Query ( "2 2 2 0"  ) == false );
  assert ( o . Query ( "2 2 2 0 0"  ) == false );
  assert ( o . Query ( "2 2 2 0 0 0"  ) == false );
  assert ( o . Query ( "2 2 2 0 0 0 0" ) == false );
  cout << "Tree \"2 0 2 0 2 0 0\" ok " << endl;

   o . BuildIndex ( "2 2 2 0 2 0 1 0 0 0"s );
  assert ( o . Query ( "2 2 2 0 2 0 1 0 0 0"  ) == true );
  assert ( o . Query ( "2 2 0 2 0 1 0 0 "  ) == true );
  assert ( o . Query ( "2 0 2 0 1 0" ) == true );
  assert ( o . Query ( "2 0 1 0" ) == true );
  assert ( o . Query ( "0" ) == true );
  assert ( o . Query ( "1 0" ) == true );
  assert ( o . Query ( "2 0 0" ) == false );
  assert ( o . Query ( "2 0 2 0 0" ) == false );
  assert ( o . Query ( "2 2 2  0" ) == false );
  assert ( o . Query ( "2 2 0 2 0 0 0 " ) == false );
  assert ( o . Query ( "2 2 2 0 1 0 0 0" ) == true );
  assert ( o . Query ( "2 2 0 1 0 0" ) == true );
  assert ( o . Query ( "2 2 2 0 2  0 0" ) == false );
  assert ( o . Query ( "2 2 2 0 0 0" ) == false );
  assert ( o . Query ( "2  2 2 0 0 0" ) == false );
  cout << "Tree \"2 2 2 0 2 0 1 0 0 0\" ok " << endl;
  


  cout << "OraclePDA ok " << endl;