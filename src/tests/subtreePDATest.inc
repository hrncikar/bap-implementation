  CSubtreePDA a;
  a. BuildIndex ( "2 1 0 0"s );
  assert ( a. Query ( "2 1 0 0" ) == true );
  assert ( a. Query ( " 1 0" ) == true );
  assert ( a. Query ( " 0" ) == true );
  assert ( a. Query ( " 0 0" ) == false );
  assert ( a. Query ( "2 1 0 " ) == false );
  cout << "Tree \"2 1 0 0\" ok" << endl;

  a. BuildIndex ( "2 0 0"s );
  assert ( a. Query ( "2 0 0"  ) == true );
  assert ( a. Query ( "0"  ) == true );
  assert ( a. Query ( "0 0 0"  ) == false );
  assert ( a. Query ( "0 0" ) == false );
  assert ( a. Query ( "2 2 2" ) == false );
  assert ( a. Query ( "1" ) == false );
  assert ( a. Query ( "1 2 3" ) == false );
  cout << "Tree \"2 0 0\" ok" << endl;
  
  a. BuildIndex ( "3 0 2 0 0 0"s );
  assert ( a. Query (  "3 0 2 0 0 0"  ) == true );
  assert ( a. Query (  "2 0 0" ) == true );
  assert ( a. Query (  "0" ) == true );
  assert ( a. Query (  "2 0 0 0" ) == false );
  assert ( a. Query (  "3 2 2 0 0 0" )  == false );
  assert ( a. Query (  "3 0 2 0 0 0 0"  ) == false );
  assert ( a. Query (  "0 0" )  == false );
  cout << "Tree \"3 0 2 0 0 0\" ok " << endl;

  a. BuildIndex ( "1 1 1 1 1 0"s );
  assert ( a. Query ( "1 1 1 1 1 0"  ) == true );
  assert ( a. Query ( "0" )  == true );
  assert ( a. Query ( "1 0"  ) == true );
  assert ( a. Query ( "1 1 1 0"  ) == true );
  assert ( a. Query ( "1" )  == false );
  assert ( a. Query ( "1 1"  ) == false );
  assert ( a. Query ( "1 1 1 1 1 1 0"  ) == false );
  cout << "Tree \"1 1 1 1 1 0\" ok " << endl;

  a. BuildIndex ( "6 0 0 0 0 0 0"s );
  assert ( a. Query ( "6 0 0 0 0 0 0" ) == true );
  assert ( a. Query ( "0" ) == true );
  assert ( a. Query ( "6 "  ) == false );
  assert ( a. Query ( "0 0" ) == false );
  assert ( a. Query ( "6 0 0 0 0 0 0 0 " ) == false );
  assert ( a. Query ( "6 0 0 0 " ) == false );
  cout << "Tree \"6 0 0 0 0 0 0\" ok " << endl;


  a. BuildIndex ( "0"s );
  assert ( a. Query ( "0" ) == true );
  assert ( a. Query ( "0 0" ) == false );
  cout << "Tree \"0\" ok " << endl;

  a. BuildIndex ( "2 2 0 0 1 0"s );
  assert ( a. Query ( "2 2 0 0 1 0"  ) == true );
  assert ( a. Query ( "2 0 0"  ) == true );
  assert ( a. Query ( "1 0"  ) == true );
  assert ( a. Query ( "0"  ) == true );
  assert ( a. Query ( "0 0" ) == false );
  assert ( a. Query ( "2 0 0 1 0"  ) == false );
  assert ( a. Query ( "2 0 0 1" ) == false );
  cout << "Tree \"2 2 0 0 1 0\" ok " << endl;

  a. BuildIndex ( "2 2 0 1 0 1 0"s );
  assert ( a. Query ( "2 2 0 1 0 1 0"  ) == true );
  assert ( a. Query ( "2 0 1 0" )  == true );
  assert ( a. Query ( "1 0" )  == true );
  assert ( a. Query ( "0" )  == true );
  assert ( a. Query ( "0 0"  ) == false );
  assert ( a. Query ( "2 0 0 1 0"  ) == false );
  assert ( a. Query ( "2 0 0 1"  ) == false );
  assert ( a. Query ( "2 0 1 0 1 0" ) == false );
  assert ( a. Query ( "2 0 0" ) == false );
  assert ( a. Query ( "2 0 0 1" ) == false );
  cout << "Tree \"2 2 0 1 0 1 0\" ok " << endl;

  a. BuildIndex ( "2 2 0 0 0"s );
  assert ( a. Query ( "2 2 0 0 0"  ) == true );
  assert ( a. Query ( "2 0 0" )  == true );
  assert ( a. Query ( "0" )  == true );
  assert ( a. Query ( "2 0 0 0"  ) == false );
  assert ( a. Query ( "2 2 0 0 0 0" ) == false );
  cout << "Tree \"2 2 0 0 1 0\" ok " << endl;

  a. BuildIndex ( "3 2 0 0 0 1 0"s );
  assert ( a. Query ( "3 2 0 0 0 1 0" ) == true );
  assert ( a. Query ( "2 0 0" ) == true );
  assert ( a. Query ( "1 0" )  == true );
  assert ( a. Query ( "0" )  == true );
  assert ( a. Query ( "0 0"  ) == false );
  assert ( a. Query ( "2 0 0 1 0"  ) == false );
  assert ( a. Query ( "3 0 0 0" ) == false );
  assert ( a. Query ( "3 0 0 1 0" ) == false );
  cout << "Tree \"3 2 0 0 0 1 0\" ok " << endl;

  a. BuildIndex ( "2 0 2 0 2 0 0"s );
  assert ( a. Query ( "2 0 2 0 2 0 0"  ) == true );
  assert ( a. Query ( "2 0 2 0 0" )  == true );
  assert ( a. Query ( "2 0 0" )  == true );
  assert ( a. Query ( "0" )  == true );
  assert ( a. Query ( "2 2 0"  ) == false );
  assert ( a. Query ( "2 2 0 0"  ) == false );
  assert ( a. Query ( "2 2 0 0 0"  ) == false );
  assert ( a. Query ( "2 2 0 0 0 0"  ) == false );
  assert ( a. Query ( "2 2 2 0"  ) == false );
  assert ( a. Query ( "2 2 2 0 0"  ) == false );
  assert ( a. Query ( "2 2 2 0 0 0"  ) == false );
  assert ( a. Query ( "2 2 2 0 0 0 0" ) == false );
  cout << "Tree \"2 0 2 0 2 0 0\" ok " << endl;

   a. BuildIndex ( "2 2 2 0 2 0 1 0 0 0"s );
  assert ( a. Query ( "2 2 2 0 2 0 1 0 0 0"  ) == true );
  assert ( a. Query ( "2 2 0 2 0 1 0 0 "  ) == true );
  assert ( a. Query ( "2 0 2 0 1 0" ) == true );
  assert ( a. Query ( "2 0 1 0" ) == true );
  assert ( a. Query ( "0" ) == true );
  assert ( a. Query ( "1 0" ) == true );
  assert ( a. Query ( "2 0 0" ) == false );
  assert ( a. Query ( "2 0 2 0 0" ) == false );
  assert ( a. Query ( "2 2 2  0" ) == false );
  assert ( a. Query ( "2 2 0 2 0 0 0 " ) == false );
  assert ( a. Query ( "2 2 2 0 1 0 0 0" ) == false );
  assert ( a. Query ( "2 2 0 1 0 0" ) == false );
  assert ( a. Query ( "2 2 2 0 2  0 0" ) == false );
  assert ( a. Query ( "2 2 2 0 0 0" ) == false );
  assert ( a. Query ( "2  2 2 0 0 0" ) == false );
  cout << "Tree \"2 2 2 0 2 0 1 0 0 0\" ok " << endl;

  cout << "SubtreePDA ok " << endl;