 
 // These do not work anymore, CCounterAutomata is an abstract class
  CCounterAutomata automat;
  map<TFrom, set <TTo>> delta1;
  delta1[{{0},2,1}]={{{1},2}};
  delta1[{{1},0,1}]={{{2},0}};
  delta1[{{2},0,1}]={{{3},0}};
  delta1[{{0},0,1}]={{{4},0}};

  automat . BuildAutomaton ( {{0},{1},{2},{3},{4}}, {2,0}, move ( delta1 ), {0}, 1);
  assert ( automat . Query ( move ( "2 0 0" ) ) == true );
  assert ( automat . Query ( move ( "0" ) ) == true );
  assert ( automat . Query ( move ( "0 0 0" ) ) == false );
  assert ( automat . Query ( move ( "0 0" ) ) == false );
  assert ( automat . Query ( move ( "2 2 2" ) ) == false );
  assert ( automat . Query ( move ( "1" ) ) == false );
  assert ( automat . Query ( move ( "1 2 3" ) ) == false );
  cout << "Tree \"2 0 0\" ok" << endl;
  //-----------------------------------------------------------------------------------------------
  map<TFrom, set <TTo>> delta2;
  delta2[{{0},3,1}]={{{1},3}};
  delta2[{{1},0,1}]={{{2},0}};
  delta2[{{2},2,1}]={{{3},2}};
  delta2[{{3},0,1}]={{{4},0}};
  delta2[{{4},0,1}]={{{5},0}};
  delta2[{{5},0,1}]={{{6},0}};
  delta2[{{0},2,1}]={{{7},2}};
  delta2[{{7},0,1}]={{{8},0}};
  delta2[{{8},0,1}]={{{9},0}};
  delta2[{{0},0,1}]={{{10},0}};   

  automat . BuildAutomaton ( {{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}}, {3,2,0}, move ( delta2 ) , {0}, 1);
  assert ( automat . Query ( move ( "3 0 2 0 0 0" ) ) == true );
  assert ( automat . Query ( move ( "2 0 0" ) ) == true );
  assert ( automat . Query ( move ( "0" ) ) == true );
  assert ( automat . Query ( move ( "2 0 0 0" ) ) == false );
  assert ( automat . Query ( move ( "3 2 2 0 0 0" ) ) == false );
  assert ( automat . Query ( move ( "3 0 2 0 0 0 0" ) ) == false );
  assert ( automat . Query ( move ( "0 0" ) ) == false );
  cout << "Tree \"3 0 2 0 0 0\" ok " << endl;
  //-----------------------------------------------------------------------------------------------