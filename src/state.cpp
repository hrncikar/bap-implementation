#include "state.hpp"

using namespace std;
//=================================================================================================
                   CState::CState                          ( const CState & copy )
{
  m_Label = copy . m_Label;
}
//-------------------------------------------------------------------------------------------------
                   CState::CState                          ( const set<int>  & copy )
{
  m_Label = copy;
}
//-------------------------------------------------------------------------------------------------
bool               CState::operator <                      ( const CState & other ) const
{
  return m_Label < other . m_Label;
}
//-------------------------------------------------------------------------------------------------
bool               CState::operator ==                     ( const CState & other ) const
{
  return m_Label == other . m_Label;
}
//-------------------------------------------------------------------------------------------------
const set<int>   & CState::Name                            ( void ) const
{ 
  return m_Label;
}
//-------------------------------------------------------------------------------------------------
std::set<int>    & CState::Insert                          ( int               addName )
{
  m_Label . insert ( addName );
  return m_Label;
}
//-------------------------------------------------------------------------------------------------
bool               CState::Empty                           ( void ) const
{
  return m_Label . empty ();
}
//-------------------------------------------------------------------------------------------------
size_t             CState::Size                            ( void ) const
{
  return m_Label . size ();
}
//-------------------------------------------------------------------------------------------------
bool               CState::AreCorresponding                ( const CState & other ) const
{
  return *( m_Label . begin () ) == *( other . m_Label . begin () );
}
//=================================================================================================