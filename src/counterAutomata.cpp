#include <sstream>
#include <iostream>
#include <queue>
#include <vector>
#include <algorithm>
#include <memory>

#include "counterAutomata.hpp"
#include "log.hpp"

using namespace std;
//=================================================================================================
bool               CCounterAutomata::Query                 ( const std::string & queryPattern )
{
  int    scounter = Z_0;
  CState cstate   = m_q0;
  stringstream iss ( queryPattern );
  int inputSymbol;

  while ( iss >> inputSymbol )
  {
    TFrom from ( cstate, inputSymbol, PARENTS );
    
    auto x = m_Delta . find ( from );
    if (( x == m_Delta . end () ) || ( x -> second . size () != 1 ))
      return false;

    if ( ! tryMove ( x, cstate, scounter ) )
      return false;
  }

  return ! scounter;
}
//-------------------------------------------------------------------------------------------------
void               CCounterAutomata::LogInformation        ( void )
{
  LOG ( " % % ", m_Q . size (), m_Delta . size () );
}
//-------------------------------------------------------------------------------------------------
void               CCounterAutomata::Determinize           ( void )
{
  // algorithm 3 from the paper
  // requirements
  map<TFrom, set <TTo>> newDelta;
  set<CState>           newQ;
  map<CState, set<int>>   sc;                              //stack content

  const auto cmp = [] ( const CState & left, const CState & right)
                                         { return right < left; };
  priority_queue<CState, vector<CState>, decltype(cmp) > unmarked { cmp };

  // inicialization
  newQ . insert ( m_q0 );
  sc [ m_q0 ] = { PARENTS };
  unmarked . push ( m_q0 );


  // the steps
  while ( ! unmarked . empty () )
  {
    CState state = unmarked . top ();
    unmarked . pop ();
    for ( int a : m_A )
    {
      CState newState;
      for ( int q : state . Name () )                      // find the state to which the transition moves
      {
        TFrom from { CState ({ q }), a, PARENTS };
        if ( m_Delta . find ( from ) == m_Delta . end () )
          continue;

        for ( const auto & to : m_Delta [ from ] )
          for ( int i : to . m_State . Name () )
            newState . Insert ( i );
      }

      if ( newState . Empty ( ) )
        continue ;

      if ( newQ . find ( newState ) == newQ . end () )
      {
        newQ . insert ( newState );
        unmarked . push ( newState );
      }

      newDelta[ {state, a, PARENTS} ] = { { newState, a } };

      for ( int count : sc . at ( state ) )                // calculate sc of the new state
        sc [ newState ] . insert (  count - PARENTS < 0 ?
                                                     -1 : 
                                                     ( count - PARENTS + a ) );
    }
  }

  swap ( m_Q, newQ );
  swap ( m_Delta, newDelta );

  deleteInaccessible ( sc );
}
//-------------------------------------------------------------------------------------------------
void               CCounterAutomata::deleteInaccessible    ( std::map<CState,std::set<int>> & sc )
{
  for ( const auto & stateItem : sc )
  {
    // inaccessible states
    if ( stateItem . second . size ( ) == 1 &&  ( * ( stateItem . second . begin () ) < 0 ) )
    {
      deleteOutgoing ( stateItem . first );
      m_Q . erase ( stateItem . first );
    }

    // inaccessible transitions
    if ( stateItem . second . size ( ) == 1 &&  ( * ( stateItem . second . begin () ) == 0 ) )
      deleteOutgoing ( stateItem . first );
  }
}
//-------------------------------------------------------------------------------------------------
void               CCounterAutomata::deleteIngoing         ( const CState    & state )
{
  for ( auto & [ from, allTos ] : m_Delta )
    for ( auto to = allTos . begin (); to != allTos . end ();  )
      if ( to -> m_State == state )
        to = allTos . erase ( to );
      else 
        ++to;
}
//-------------------------------------------------------------------------------------------------
void               CCounterAutomata::deleteOutgoing        ( const CState    & state )
{
  for ( int a : m_A )
    m_Delta . erase ( { state, a, PARENTS } );
}
//-------------------------------------------------------------------------------------------------
void               CCounterAutomata::copyIngoing           ( const CState    & fromState,
                                                             const CState    & toState )
{
  for ( auto & transition : m_Delta )
    for ( const auto & to : transition . second )
      if ( to . m_State == fromState )
        m_Delta[transition . first] . insert ( {toState, to . m_TopOfStack });
}
//-------------------------------------------------------------------------------------------------
void               CCounterAutomata::copyOutgoing          ( const CState    & from,
                                                             const CState    & to )
{
  for ( auto a : m_A )
    if ( m_Delta . find ( { from , a, PARENTS }) != m_Delta . end () )
        for ( const auto & transition : m_Delta[ { from , a, PARENTS } ] )
          m_Delta[{to, a, PARENTS}] . insert ( transition );
}
//-------------------------------------------------------------------------------------------------
bool               CCounterAutomata::tryMove               ( map<TFrom, set<TTo>>::iterator from, // perhaps a bit uglier, but we save one log(n) search
                                                             CState          & state,
                                                             int             & stackCounter )
{
  const TTo & to = *( from -> second . begin () );
  stackCounter -= from -> first . m_TopOfStack;
  if ( stackCounter < 0 )
    return false;
  stackCounter += to . m_TopOfStack;
  state = to . m_State;
  return true;
}
//-------------------------------------------------------------------------------------------------
void               CCounterAutomata::clear                 ( void )
{
  m_Delta . clear ();
  m_Q . clear ();
  m_A . clear ();
}
//=================================================================================================
                   CCounterAutomata::TFrom::TFrom          ( const CState    & state,
                                                             int               inputSymbol,
                                                             int               topOfStack )
: m_State ( state ),
  m_InputSymbol ( inputSymbol ),
  m_TopOfStack ( topOfStack )
{}
//-------------------------------------------------------------------------------------------------                      
bool               CCounterAutomata::TFrom::operator <     ( const TFrom     & other ) const
{
  if ( m_State == other . m_State )
    return m_InputSymbol < other . m_InputSymbol;
  return m_State < other . m_State;
}
//=================================================================================================
                   CCounterAutomata::TTo::TTo              ( const CState    & state,
                                                             int               topOfStack )
: m_State ( state ),
  m_TopOfStack ( topOfStack )
{}
//-------------------------------------------------------------------------------------------------
bool               CCounterAutomata::TTo::operator <       ( const TTo       & other ) const
{
  return m_State < other . m_State;
}
//=================================================================================================