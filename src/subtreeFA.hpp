#ifndef BAP_SUBTREEFA_HPP
#define BAP_SUBTREEFA_HPP

#include <sstream>
#include <set>
#include <string>
#include <vector>


#include "deterministicFA.hpp"

//=================================================================================================
/**
 * CSubtreeFA class represent a subtree finite automaton. Before using it it has to be built by
 * BuildIndex(tree).
 */
class CSubtreeFA : public CDeterministicFA
{
  public:
    virtual                           ~CSubtreeFA          ( void ) = default;
    /**
     * BuildIndex creates a deterministic subtree finite automaton for a given tree.
     * @param[in] tree is the linear notation of a ranked tree for which the index is build
     */
    virtual void                       BuildIndex          ( const std::string & tree ) override;
  
  protected:
    std::set<std::vector<int>>         subtreeSet          ( const std::string & tree );
};
//=================================================================================================
#endif //BAP_SUBTREEFA_HPP