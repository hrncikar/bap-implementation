#include <sstream>
#include <iostream>

#include "subtreePDA.hpp"

using namespace std;

//=================================================================================================
void               CSubtreePDA::BuildIndex                 ( const std::string & tree )
{
  clear ();
  
  // algorithm 2 from the paper
  // inicialization
  m_q0 = CState ( {0} );
  m_Q . insert ( m_q0 );

  stringstream iss ( tree );
  int node;
  int newStateName = 1;
  CState newState ({ newStateName });

  // root node
  iss >> node;
  m_A . insert ( node );
  m_Q . insert ( newState );
  m_Delta[{ m_q0, node , PARENTS }] . insert ( { newState, node } );

  // all other nodes
  while ( iss >> node )
  {
    m_A . insert ( node );
    newState = CState ({ ++newStateName });
    m_Q . insert ( newState );
    m_Delta[{ CState ({ newStateName - 1 }), node , PARENTS }] . insert ( { newState, node } );
    m_Delta[{ m_q0, node , PARENTS }] . insert ( { newState, node } );
  }
  
  // calling algorithm 3
  Determinize ();
}
//=================================================================================================
 