#include <iostream>
#include <fstream>
#include <string>
#include <assert.h>
#include <memory>
#include <chrono>
#include <vector>

#include "subtreePDA.hpp"
#include "subtreeFA.hpp"
#include "oraclePDA.hpp"
#include "timer.hpp"

#define QUERIES_ORDER 3
static const char * EXPERIMENT_FILE_NAME = "data/experiment/results/log.txt";

using namespace std;

bool                                   loadQueryFile       ( int                                   count,
                                                             char                                * argv[],
                                                             vector<string>                      & queries )
{
  ifstream queryF ( argv[count] );
  if ( ! queryF . is_open () )
  {
    cout << "couldn't open a query file" << endl;
    return false;
  }

  for ( string query; getline ( queryF, query ); )
    queries . push_back ( query );

  return true;
}
//-------------------------------------------------------------------------------------------------
bool                                   parseArg            ( int                                   argc,
                                                             char                                * argv[],
                                                             unique_ptr<CSubtreeRejectionIndex>  & index,
                                                             vector<string>                      & trees,
                                                             vector<string>                      & queries,
                                                             unique_ptr<ofstream>                & fout )
{
  if ( argc < 4 )
  {
    cout << "Expected at least four arguments 4 " << argc << endl;
    cout << argv[0] << " <automata> <treeinputfile> ( -q X X * <queryinputfile> | <queryinputfile> ) [<experimentoutputfile>]" << endl;
    return false;
  }
  
  // Automata
  if ( argv[1] == "subtreePDA"s )
    index = make_unique<CSubtreePDA> ();
  else if ( argv[1] == "subtreeFA"s )
    index = make_unique<CSubtreeFA> ();
  else if ( argv[1]== "subtreeOraclePDA"s )
    index = make_unique<COraclePDA> ();
  else
  {
    cout << "automata expected to be either subtreePDA, subtreeFA, or subtreeOraclePDA" << endl;
    return false;
  }

  // Trees
  ifstream treesF ( argv[2] );
  if ( ! ( treesF . is_open () ) )
  {
    cout << "couldn't open the trees file" << endl;
    return false;
  }
  for ( string tree; getline ( treesF, tree ); )
    trees . push_back ( tree );

  // Queries
  int count = 3;
  if ( argv[count] == "-q"s )
  {
    int numberOfFiles = stoi ( argv[++count] );
    for ( ++count ; count <= QUERIES_ORDER + numberOfFiles + 1; ++count )
      if ( ! loadQueryFile ( count, argv, queries ) )
        return false;
  }
  else
    if ( ! loadQueryFile ( count, argv, queries ) )
      return false;

  ++count;

  // Logging results
  if ( argc > count )
    fout = make_unique<ofstream> ( argv[count], ios::app );
  else
    fout = make_unique<ofstream> ( EXPERIMENT_FILE_NAME, ios::app );

  if ( fout -> is_open () )
      CLog::Init ( *fout );
  return true;
}
//-------------------------------------------------------------------------------------------------
int                                    main                ( int                                   argc,
                                                             char                                * argv[] ) 
{
  unique_ptr<CSubtreeRejectionIndex> index;
  vector<string> trees;
  vector<string> queries;
  unique_ptr<ofstream> fout; 


  if ( ! parseArg ( argc, argv, index, trees, queries, fout ) )
    return 1;

  // Experiment
  for ( const auto & tree : trees )
  {
    {
      CTimer buildTimer ( " % " );
      index -> BuildIndex (  tree  );
    }

    index -> LogInformation ();
    {
      CTimer queryTimer ( " %\n" );
      for ( const auto & query : queries )
      {
        cout << index -> Query ( query ) << endl;
        for ( int i = 0; i < 9; ++i ) //run each query more times
          index -> Query ( query ); 
      }
    }
  }

  //#include "./tests/subtreePDATest.inc"
  //#include "./tests/oraclePDATest.inc"
  //#include "./tests/subtreeFATest.inc"

  return 0;
}