#ifndef BAP_TIMER_HPP
#define BAP_TIMER_HPP

#include <iostream>
#include <list>
#include <memory>
#include <functional>
#include <ctime>

#include "log.hpp"

#define CONVERT_BY 1000.0

//=================================================================================================
class CTimer
{
  public:
                                       CTimer              ( const char      * logString )
    : m_LogString ( logString ),
      m_Start ( std::clock () )
    { }
                                      ~CTimer              ( void )
    {
      [[maybe_unused]] std::clock_t end = std::clock ();
      LOG ( m_LogString, CONVERT_BY * ( end - m_Start ) / CLOCKS_PER_SEC );
    }
  private:
    [[maybe_unused]] const char      * m_LogString;
    std::clock_t                       m_Start;
};
//=================================================================================================
#endif //BAP_TIMER_HPP
