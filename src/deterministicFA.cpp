#include <sstream>
#include <iostream>

#include "deterministicFA.hpp"

using namespace std;
//=================================================================================================
bool               CDeterministicFA::Query                 ( const std::string & queryPattern )
{
  CState cstate = m_q0;
  stringstream iss ( queryPattern );
  int inputSymbol;

  while ( iss >> inputSymbol )
  {
    TFrom from ( cstate, inputSymbol );

    auto x = m_Delta . find ( from );
    if ( x == m_Delta . end () )
      return false;

    cstate = x -> second . m_State;
  }

  return m_F . find ( cstate ) != m_F . end ();
}
//-------------------------------------------------------------------------------------------------
void               CDeterministicFA::LogInformation        ( void )
{
  LOG ( " % % ", m_Q . size (), m_Delta . size () );
}
//-------------------------------------------------------------------------------------------------
void               CDeterministicFA::clear                 ( void )
{
  m_Delta . clear ();
  m_Q . clear ();
  m_F . clear ();
}
//=================================================================================================
                   CDeterministicFA::TFrom::TFrom          ( const CState    & state,
                                                             int               inputSymbol )
: m_State ( state ),
  m_InputSymbol ( inputSymbol )
{}
//-------------------------------------------------------------------------------------------------                      
bool               CDeterministicFA::TFrom::operator <     ( const TFrom     & other ) const
{
  if ( m_State == other . m_State )
    return m_InputSymbol < other . m_InputSymbol;
  return m_State < other . m_State;
}
//=================================================================================================
                   CDeterministicFA::TTo::TTo              ( const CState    & state )
: m_State ( state )
{}
//-------------------------------------------------------------------------------------------------
bool               CDeterministicFA::TTo::operator <       ( const TTo       & other ) const
{
  return m_State < other . m_State;
}
//=================================================================================================
